package com.decathlon.beacon.authentication.dto;


import com.decathlon.beacon.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class AuthenticatedUser implements UserDetails{

    private User user;

    public AuthenticatedUser(User user) {
        this.user = user;
    }

    @Override

    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        return authorities;
    }


    @Override
    public String getPassword() {
        return String.valueOf(getUser().getOtp());
    }

    @Override
    public String getUsername() {
        return getUser().getMobile();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "AuthenticatedUser{" +
                "user=" + user.toString() +
                '}';
    }
}
