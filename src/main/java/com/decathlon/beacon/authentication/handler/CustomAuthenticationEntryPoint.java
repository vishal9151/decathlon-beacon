package com.decathlon.beacon.authentication.handler;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
      throws IOException, ServletException {
    /*response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");*/
    response.setStatus(HttpStatus.UNAUTHORIZED.value());
    response.setContentType(MediaType.APPLICATION_JSON_VALUE);
    response.setCharacterEncoding("UTF-8");
    JSONObject jsonResponse = new JSONObject();
    try {
      jsonResponse.put("message", HttpStatus.UNAUTHORIZED.getReasonPhrase());
      jsonResponse.put("status",HttpStatus.UNAUTHORIZED.value());
    } catch (JSONException e) {
      e.printStackTrace();
    }
    response.getWriter().write(jsonResponse.toString());
  }
}
