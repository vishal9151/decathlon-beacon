package com.decathlon.beacon.authentication.jwt;

import com.decathlon.beacon.authentication.dto.AuthenticatedUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

  private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {
    String jwtToken = (String) authentication.getCredentials();
    AuthenticatedUser user = JwtUtil.getUser(jwtToken);
    return new JwtAuthToken(user, jwtToken, user.getAuthorities());
  }

  @Override
  public boolean supports(Class<?> authentication) {
    return (JwtAuthToken.class.isAssignableFrom(authentication));
  }
}
