package com.decathlon.beacon.authentication.jwt;

import com.decathlon.beacon.authentication.dto.AuthenticatedUser;
import com.decathlon.beacon.domain.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.*;
import java.security.*;
import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Component
public class JwtUtil {

    @Autowired
    Environment environment;

    private static final Logger LOG = LoggerFactory.getLogger(JwtUtil.class);

    public static final String AUTHORIZATION = "Authorization";
    public static final String USER_AGENT = "User-Agent";
    public static final String AUTH_SERVICE_JWT = "auth-service-jwt";
    public static Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token).getBody();
    }

    public static String getToken(TokenTO tokenTO) {
        return Jwts.builder().setSubject(tokenTO.getSubject())
                .setExpiration(tokenTO.getExpirationDate())
                .setIssuer(AUTH_SERVICE_JWT)
                .claim("uid", tokenTO.getMobile())
                .claim("otp", tokenTO.getOtp())
                .setIssuedAt(new Date())
                .setNotBefore(new Date())
                .setHeaderParams(tokenTO.getHeaderClaims())
                .signWith(SignatureAlgorithm.RS256, getKey()).compact();
    }

    public static TokenTO buildTokenTO(AuthenticatedUser user) {
        LocalDateTime ldt = LocalDateTime.now().plusMinutes(720);
        Date expirationDate = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        Map<String, Object> headerClaims = new HashMap<>();
        headerClaims.put("type", "JWT");
        headerClaims.put("kid", "MAIN");
        TokenTO tokenTO = new TokenTO();
        tokenTO.setSubject(String.valueOf(user.getUser().getId()));
        tokenTO.setMobile(user.getUsername());
        tokenTO.setOtp(user.getPassword());
        tokenTO.setIssueDate(new Date());
        tokenTO.setIssuer(JwtUtil.AUTH_SERVICE_JWT);
        tokenTO.setExpirationDate(expirationDate);
        tokenTO.setHeaderClaims(headerClaims);
        return tokenTO;
    }

    public static boolean validateToken(TokenTO tokenTO) {
        try {
            Jwts.parser()
                    .setSigningKey(getKey())
                    .requireIssuer(tokenTO.getIssuer())
                    .requireSubject(tokenTO.getSubject())
                    .parseClaimsJws(tokenTO.getJwtToken());
            // @formatter:on
        } catch (JwtException | IllegalArgumentException e) {
            LOG.warn("Invalid JWT Token ->", e);
            return false;
        }
        return true;
    }

    public static AuthenticatedUser getUser(String token) {
        Claims claims = Jwts.parser().setSigningKey(getKey()).parseClaimsJws(token).getBody();
        User authenticatedUser = new User();
        authenticatedUser.setMobile(claims.getSubject());
        return new AuthenticatedUser(authenticatedUser);
    }

    private static PrivateKey getKey() {
        PrivateKey key = null;
        try {
              //FileInputStream fileInputStream = new FileInputStream("/opt/tomcat/webapps/beacon/WEB-INF/classes/PrKey.txt"); //for server
            FileInputStream fileInputStream = new FileInputStream("/usr/local/tomcat8/webapps/beacon/WEB-INF/classes/PrKey.txt"); //for production
            //FileInputStream fileInputStream = new FileInputStream("src/main/resources/PrKey.txt"); //for local

            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            key = (PrivateKey) objectInputStream.readObject();
            objectInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return key;
    }

    private static void writeKeys() {
        try {
            KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
            keyGenerator.initialize(1024);
            KeyPair kp = keyGenerator.genKeyPair();
            RSAPublicKey publicKey = (RSAPublicKey) kp.getPublic();
            PrivateKey privateKey = kp.getPrivate();
            writePemFile(privateKey, "RSA Private Key", "PrivateKey");
            writePemFile(publicKey, "RSA Public Key", "PublicKey");
            FileOutputStream fileOutputStream = new FileOutputStream("/resources/PrKey.txt");
            ObjectOutputStream objectOutputStream
                    = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(privateKey);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (IOException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private static void writePemFile(Key key, String description, String filename)
            throws IOException {
        PemFile pemFile = new PemFile(key, description);
        pemFile.write(filename);
        LOG.info(String.format("%s successfully writen in file %s.", description, filename));
    }

}
