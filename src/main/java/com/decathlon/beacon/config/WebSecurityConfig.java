package com.decathlon.beacon.config;

import com.decathlon.beacon.authentication.handler.CustomAccessDeniedHandler;
import com.decathlon.beacon.authentication.handler.CustomAuthenticationEntryPoint;
import com.decathlon.beacon.authentication.handler.CustomLogoutSuccessfulHandler;
import com.decathlon.beacon.authentication.jwt.JwtAuthenticationFilter;
import com.decathlon.beacon.authentication.jwt.JwtAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

    @Autowired
    private CustomLogoutSuccessfulHandler logoutSuccessfulHandler;

    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;

    @Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private AuthenticationManager authenticationManager;

    protected JwtAuthenticationFilter getJwtAuthenticationFilter(String pattern) {
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter(new AntPathRequestMatcher(pattern));
        jwtAuthenticationFilter.setAuthenticationManager(this.authenticationManager);
        return jwtAuthenticationFilter;

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .csrf().disable()
            .authorizeRequests()
                .antMatchers("/api/v1/**")
                .permitAll()
                .anyRequest().authenticated()
            .and()
                .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler)
                .authenticationEntryPoint(customAuthenticationEntryPoint)
            .and()
                .logout()
                .deleteCookies("JSESSIONID")
                .logoutUrl("/auth/logout")
                .logoutSuccessHandler(logoutSuccessfulHandler)
            .and()
                .addFilterBefore(new CORSFilterConfig(), ChannelProcessingFilter.class)
                .addFilterBefore(getJwtAuthenticationFilter("/api/one_id/**"),UsernamePasswordAuthenticationFilter.class);


    }

    @Override
    public void configure(WebSecurity web) throws Exception {
                web
                    .ignoring()
                        .antMatchers(
                                "/v2/api-docs",
                                "/configuration/ui",
                                "/swagger-resources",
                                "/configuration/security",
                                "/swagger-ui.html",
                                "/webjars/**"
                        );
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
