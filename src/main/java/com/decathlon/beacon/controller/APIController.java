package com.decathlon.beacon.controller;

import com.decathlon.beacon.domain.category.Category;
import com.decathlon.beacon.domain.category.CategoryAssembler;
import com.decathlon.beacon.domain.category.CategoryDTO;
import com.decathlon.beacon.domain.oneid.constants.OneIdConstants;
import com.decathlon.beacon.domain.oneid.create.CreateCustomer;
import com.decathlon.beacon.domain.oneid.search_customer.SearchCustomerDTO;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateCustomerContact;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateIdentityDTO;
import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.domain.product.ProductAssembler;
import com.decathlon.beacon.domain.product.ProductDTO;
import com.decathlon.beacon.models.DummyReq;
import com.decathlon.beacon.models.OrderDetails;
import com.decathlon.beacon.models.Result;
import com.decathlon.beacon.services.CommonService;
import com.decathlon.beacon.util.Constants;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by VISHAL on 26-12-2017.
 */

@RestController
@RequestMapping("/api/v1")
public class APIController {

    @Autowired
    CommonService commonService;

    @Autowired
    Environment environment;

    @GetMapping(value = "checkIsItAlive")
    public ResponseEntity<String> isBeaconServicesAlive() {
        String[] activeProfiles = environment.getActiveProfiles();
        StringBuilder stringBuilder = new StringBuilder();
        if (activeProfiles != null) {
            for (String profile : activeProfiles) {
                stringBuilder.append(profile + " ");
            }
        }
        return new ResponseEntity<String>("Welcome to Beacon " + stringBuilder.toString().trim() + " services.", HttpStatus.OK);
    }

    public static File getFile(@RequestParam("file") MultipartFile file) throws IOException {
        File productMasterFile = new File(System.getProperty("catalina.base") + File.separator + file.getOriginalFilename());
        OutputStream outputStream = new FileOutputStream(productMasterFile);
        InputStream inputStream = file.getInputStream();
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        return productMasterFile;
    }

    @RequestMapping(value = {"/categories"}, method = RequestMethod.GET)
    public ResponseEntity<Result> getAllCategories() {
        Result result = commonService.getAllCategories();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/placemarks"}, method = RequestMethod.GET)
    public ResponseEntity<Result> getAllPlacemarks() {
        Result result = commonService.getAllPlacemarks();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/sub_categories_by_category"}, method = RequestMethod.GET)
    public ResponseEntity<Result> getSubCategoriesByCategory(@RequestParam("categoryId") int categoryId) {
        Result result = commonService.getSubCategoriesByCategory(categoryId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/products_by_sub_category"}, method = RequestMethod.GET)
    public ResponseEntity<Result> getProductsBySubCategory(@RequestParam("subCategoryId") int subCategoryId) {
        Result result = commonService.getProductsBySubCategory(subCategoryId);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/search_product"}, method = RequestMethod.GET)
    public ResponseEntity<Result> getProductsBySubCategory(@RequestParam("currentPage") int currentPage,
                                                           @RequestParam("pageSize") int pageSize,
                                                           @RequestParam("searchString") String searchString) {
        Result result = commonService.searchProducts(currentPage, pageSize, searchString);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /*@RequestMapping(value = {"/update_products"}, method = RequestMethod.GET)
    public ResponseEntity<Result> updateProducts(HttpSession session) {
        Result result = commonService.updateProducts();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }*/

    @RequestMapping(value = {"/save_category"}, method = RequestMethod.GET)
    public ResponseEntity<Result> saveCategories(HttpSession session) {
        Result result = commonService.saveCategories();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/update_product_placemark"}, method = RequestMethod.POST)
    public ResponseEntity<Result> updateProductPlacemark(@RequestParam("modelCode") String modelCode,
                                                         @RequestParam("placemarkId") String placemarkId) {
        Result result = new Result();
        try {
            commonService.updateInventory(modelCode, placemarkId);
            if (modelCode != null && placemarkId != null &&
                    !modelCode.isEmpty() && !placemarkId.isEmpty()) {
                result = commonService.updateProductPlacemarks(modelCode, placemarkId);
                if (result != null)
                    return new ResponseEntity<Result>(result, HttpStatus.OK);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }

    @RequestMapping(value = {"/send_otp"}, method = RequestMethod.POST)
    public ResponseEntity<Result> sendOtp(@RequestParam("mobile") String mobile) {
        Result result = new Result();
        try {
            result = commonService.sendSms(mobile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/verify_otp"}, method = RequestMethod.POST)
    public ResponseEntity<Result> verifyOtp(@RequestParam("mobile") String mobile,
                                            @RequestParam("otp") int otp) {
        Result result = new Result();
        try {
            result = commonService.validateOtp(mobile, otp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @PostMapping(value = "/order-confirmation-sms")
    public ResponseEntity<Result> orderConfirmationSMS(@RequestBody OrderDetails orderDetails){
        Result result = new Result();
        try{
            if(orderDetails!=null){
                result= commonService.sendOrderConfirmationSMS(orderDetails);
                if(result!=null)
                    return new ResponseEntity<Result>(result,HttpStatus.OK);
                else

                    return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }


    @GetMapping(value = "/subcategories/{categoryID}")
    public ResponseEntity<Result> subCategoriesByCategoryID(@RequestParam String categoryID) {
        Result result = new Result();
        try {
            if (categoryID != null && !categoryID.isEmpty()) {
//                String rootCategory= Constants.ALL_SPORTS_CATEGORY_ID;
                Category category = commonService.getCategoryByCategoryID(categoryID);
                List<CategoryDTO> categoryDTOList = new ArrayList<CategoryDTO>();
                if (category != null && category.getSubCategories() != null && category.getSubCategories().size() > 0) {
                    for (Category subCategory : category.getSubCategories()) {
                        if (Constants.CATEGORY_IDS_FOR_UPDATE.indexOf(subCategory.getCategoryID()) > -1) {
                            continue;
                        }
                        if (commonService.checkProductAvailability(subCategory)) {
                            CategoryDTO categoryDTO =
                                    CategoryAssembler.getCategoryDTOFromCategory(subCategory);
                            categoryDTOList.add(categoryDTO);
                        }
                    }
                    if (categoryDTOList.size() > 0) {
                        result.setMessage(categoryDTOList.size() + " Subcategories");
                        result.setStatus(true);
                        result.setResultObject(categoryDTOList);
                        return new ResponseEntity<>(result, HttpStatus.OK);

                    } else {
                        result.setMessage("No Subcategories");
                        result.setStatus(true);
                        result.setResultObject(null);
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    }
                } else {
                    result.setMessage("No Subcategories");
                    result.setStatus(false);
                    result.setResultObject(null);
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);

    }

    @GetMapping(value = "/products/search/{searchParam}")
    public ResponseEntity<Result> searchProducts(@RequestParam String searchParam) {
        Result result = new Result();
        try {
            if (searchParam != null && !searchParam.isEmpty()) {
                List<Product> productList = commonService.searchProduct(searchParam);
                if (productList != null && productList.size() > 0) {
                    List<ProductDTO> productDTOS = new ArrayList<>();
                    for (Product product : productList) {
                        if (product.getStatus() == 1 && product.getArubaPlacemarkId() != null) {
                            ProductDTO productDTO = ProductAssembler.getProductDTOFromProduct(product);
                            productDTOS.add(productDTO);
                        }
                    }
                    if (productDTOS.size() > 0) {
                        result.setMessage(productDTOS.size() + " products");
                        result.setStatus(true);
                        result.setResultObject(productDTOS);
                        return new ResponseEntity<>(result, HttpStatus.OK);

                    } else {
                        result.setMessage("No active products found for search parameter.");
                        result.setStatus(false);
                        result.setResultObject(null);
                        return new ResponseEntity<>(result, HttpStatus.OK);
                    }
                } else {
                    result.setMessage("No products found for search parameter.");
                    result.setStatus(false);
                    result.setResultObject(null);
                    return new ResponseEntity<>(result, HttpStatus.OK);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "/placemark_by_model_code")
    public ResponseEntity<Result> getPlacemarkByModelCode(@RequestParam String modelCode) {
        Result result = new Result();
        try {
            result = commonService.getPlacemarkByModelCode(modelCode);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
