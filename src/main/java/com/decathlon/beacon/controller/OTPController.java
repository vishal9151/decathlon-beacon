/*
package com.decathlon.beacon.controller;

import com.decathlon.beacon.models.Result;
import com.decathlon.beacon.services.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/otp")
public class OTPController {
    @Autowired
    CommonService commonService;

    @Autowired
    Environment environment;

    @RequestMapping(value = {"/send_otp"}, method = RequestMethod.POST)
    public ResponseEntity<Result> sendOtp(@RequestParam("mobile") String mobile) {
        Result result = new Result();
        try {
            result = commonService.sendSms(mobile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @RequestMapping(value = {"/verify_otp"}, method = RequestMethod.POST)
    public ResponseEntity<Result> verifyOtp(@RequestParam("mobile") String mobile,
                                            @RequestParam("otp") int otp) {
        Result result = new Result();
        ResponseEntity responseEntity = null;
        try {
            result = commonService.validateOtp(mobile, otp);
            if (result!=null){
                responseEntity = new ResponseEntity(result,result.getStatusCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity(result,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }
    @GetMapping(value = "checkIsItAlive")
    public ResponseEntity<String> isBeaconServicesAlive() {
        String[] activeProfiles = environment.getActiveProfiles();
        StringBuilder stringBuilder = new StringBuilder();
        if (activeProfiles != null) {
            for (String profile : activeProfiles) {
                stringBuilder.append(profile + " ");
            }
        }
        return new ResponseEntity<String>("Welcome to Beacon " + stringBuilder.toString().trim() + " services.", HttpStatus.OK);
    }
}
*/
