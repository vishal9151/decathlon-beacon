package com.decathlon.beacon.controller;

import com.decathlon.beacon.domain.oneid.constants.OneIdConstants;
import com.decathlon.beacon.domain.oneid.create.CreateCustomer;
import com.decathlon.beacon.domain.oneid.search_customer.SearchCustomerDTO;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateCustomerContact;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateIdentityDTO;
import com.decathlon.beacon.models.Result;
import com.decathlon.beacon.services.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/one_id")
public class OneIdController {

    @Autowired
    CommonService commonService;

    @PostMapping(value = "/search_customer")
    public ResponseEntity<Result> getCustomerDetails(@RequestBody SearchCustomerDTO dto) {

        Result result = new Result();
        if (dto != null) {
            if (dto.getMobile() == null || dto.getMobile().isEmpty()) {
                result.setMessage("Mobile should not be empty");
                return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
            }
        }

        Boolean validUser = commonService.checkUser(dto);
        if (validUser) {
            try {
                result = commonService.getCustomerDetails(dto);
                if (result!=null){
                    return new ResponseEntity<>(result,result.getStatusCode());
                }
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            result.setMessage("Not a valid User");
            return new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
        }
        return null;
    }

    @PostMapping(value = "/create_customer")
    public ResponseEntity<Result> createCustomer(@RequestBody CreateCustomer customer) {
        Result result = new Result();
        ResponseEntity responseEntity = null;
        try {
            result = commonService.createCustomer(customer);
            if (result != null) {
                responseEntity = new ResponseEntity<>(result,result.getStatusCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            responseEntity = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @PutMapping(value = "/update_customer/identity")
    public ResponseEntity<Result> updateIdentity(@RequestBody UpdateIdentityDTO dto) {
        Result result = new Result();
        ResponseEntity responseEntity = null;

        Long idPerson = commonService.checkUser();
        if (dto != null) {
            if (dto.getId_person() != null) {
                if (idPerson.equals(dto.getId_person())) {
                    try {
                        result = commonService.updateCustomerIdentity(dto);
                        if (result != null) {
                            responseEntity = new ResponseEntity<>(result, result.getStatusCode());
                        }
                    } catch (Exception e) {
                        responseEntity = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                } else{
                    result.setMessage(OneIdConstants.FAIL);
                    responseEntity = new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
                }

            }
        }
        return responseEntity;
    }

    @PutMapping(value = "/update_customer/contact")
    public ResponseEntity<Result> updateContact(@RequestBody UpdateCustomerContact customerContact) {
        Result result = new Result();
        ResponseEntity responseEntity = null;

        if (customerContact.getId_person() == null) {
            result.setMessage("Id Person Missing");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        if (customerContact.getMobile_country_code() == null) {
            result.setMessage("Country Code is Missing");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        } else if (customerContact.getId_person() != null) {
            Long idPerson = commonService.checkUser();
            if (customerContact.getId_person().equals(idPerson)) {
                try {
                    result = commonService.updateCustomerContact(customerContact);
                    if (result != null) {
                        responseEntity = new ResponseEntity<>(result, result.getStatusCode());
                    }
                } catch (Exception e) {
                    responseEntity = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                result.setMessage("Not a Valid User");
                responseEntity = new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
            }
        }
        return responseEntity;
    }

    @PutMapping(value = "/update_customer/address")
    public ResponseEntity<Result> updateAddress(@RequestBody Map<String, Object> request) {
        Result result = new Result();
        ResponseEntity responseEntity;
        Long id_person = Long.parseLong(request.getOrDefault("id_person", "0").toString());
        if (id_person == 0) {
            result.setMessage("Id Person Missing");
            return new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
        }
        if (id_person.equals(commonService.checkUser())) {
            try {
                if (!(request.containsKey("city")) || !(request.containsKey("line1"))) {
                    result.setMessage("City and line1 address is required");
                    responseEntity = new ResponseEntity<>(result, HttpStatus.BAD_REQUEST);
                }
                result = commonService.updateCustomerAddress(request);
                responseEntity = new ResponseEntity<>(result, result.getStatusCode());
            } catch (Exception e) {
                e.printStackTrace();
                result.setMessage("Something went wrong");
                responseEntity = new ResponseEntity<>(result, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            result.setMessage("Not a Valid User");
            responseEntity = new ResponseEntity<>(result, HttpStatus.UNAUTHORIZED);
        }

        return responseEntity;
    }
}
