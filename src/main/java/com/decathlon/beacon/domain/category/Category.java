package com.decathlon.beacon.domain.category;


import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.product.Product;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by VISHAL on 26-12-2017.
 */

@Entity
@Table(name = "category")
public class Category implements Serializable{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
    private Long id;

    @Column(name = "category_id" ,nullable = false)
    private String categoryID;

    @Column(name = "hierarchical_parent_id", nullable = false)
    private String hierarchicalParentID;

    @Column(name = "placemark_id")
    private Long placemarkId;

    @Column(name = "name")
    private String name;

    @Column(name = "thumbnail_id")
    private String thumbnailID;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "parent_id")
    private Long parentID;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parent_id", nullable = false, insertable = false, updatable = false)
    private Category category;

    @ManyToOne()
    @JoinColumn(name = "placemark_id", insertable = false, updatable = false)
    private Placemark placemark;

    @OneToMany(mappedBy = "category")
    private List<Category> subCategories=new ArrayList<>();

    @Column(name = "status",length = 2)
    private int status;

    @OneToMany(
            mappedBy = "category",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Product> products=new ArrayList<>();

    @Column(name = "updated_on")
    private Timestamp updatedOn;

    @Column(name = "created_on")
    private Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbnailID() {
        return thumbnailID;
    }

    public void setThumbnailID(String thumbnailID) {
        this.thumbnailID = thumbnailID;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Category> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<Category> subCategories) {
        this.subCategories = subCategories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getHierarchicalParentID() {
        return hierarchicalParentID;
    }

    public void setHierarchicalParentID(String hierarchicalParentID) {
        this.hierarchicalParentID = hierarchicalParentID;
    }

    public Long getParentID() {
        return parentID;
    }

    public void setParentID(Long parentID) {
        this.parentID = parentID;
    }

    /**
     * Sets createdAt before insert
     */
    @PrePersist
    public void setCreationDate() {
        this.createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Sets updatedOn before update
     */
    @PreUpdate
    public void setChangeDate() {
        this.updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    public Long getPlacemarkId() {
        return placemarkId;
    }

    public void setPlacemarkId(Long placemarkId) {
        this.placemarkId = placemarkId;
    }

    public Placemark getPlacemark() {
        return placemark;
    }

    public void setPlacemark(Placemark placemark) {
        this.placemark = placemark;
    }
}
