package com.decathlon.beacon.domain.category;

import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.domain.product.ProductAssembler;
import com.decathlon.beacon.domain.product.ProductDTO;

import java.util.ArrayList;
import java.util.List;

public class CategoryAssembler {

    public static CategoryDTO getCategoryDTOFromCategory(Category category){
        if(category!=null) {
            final CategoryDTO categoryDTO = new CategoryDTO();
            categoryDTO.setId(category.getId());
            categoryDTO.setCategoryID(category.getCategoryID());
            categoryDTO.setHierarchicalParentID(category.getHierarchicalParentID());
            categoryDTO.setName(category.getName());
            categoryDTO.setThumbnailID(category.getThumbnailID());
            categoryDTO.setImageUrl(category.getImageUrl());
            categoryDTO.setParentID(category.getParentID());
            categoryDTO.setPlacemark(category.getPlacemark());
            categoryDTO.setStatus(category.getStatus());
            categoryDTO.setUpdatedOn(category.getUpdatedOn());
            categoryDTO.setCreatedOn(category.getCreatedOn());
            if(category.getProducts()!=null){
                List<ProductDTO> productDTOS=new ArrayList<>();
                for(Product product:category.getProducts()) {
                    if(product.getStatus()==1 &&
                            product.getArubaPlacemarkId()!=null
                            ) {
                        productDTOS.add(ProductAssembler.getProductDTOFromProduct(product));
                    }
                }
                categoryDTO.setProducts(productDTOS);
            }
            return categoryDTO;
        }
        return null;
    }
}
