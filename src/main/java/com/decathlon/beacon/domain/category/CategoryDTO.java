package com.decathlon.beacon.domain.category;

import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.domain.product.ProductDTO;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CategoryDTO {

    private Long id;

    private String categoryID;

    private String hierarchicalParentID;

    private Long placemarkId;

    private String name;

    private String thumbnailID;

    private String imageUrl;

    private Long parentID;

    private List<ProductDTO> products=new ArrayList<>();

    private Placemark placemark;

    private int status;

    private Timestamp updatedOn;

    private Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getHierarchicalParentID() {
        return hierarchicalParentID;
    }

    public void setHierarchicalParentID(String hierarchicalParentID) {
        this.hierarchicalParentID = hierarchicalParentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnailID() {
        return thumbnailID;
    }

    public void setThumbnailID(String thumbnailID) {
        this.thumbnailID = thumbnailID;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getParentID() {
        return parentID;
    }

    public void setParentID(Long parentID) {
        this.parentID = parentID;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public Long getPlacemarkId() {
        return placemarkId;
    }

    public void setPlacemarkId(Long placemarkId) {
        this.placemarkId = placemarkId;
    }

    public Placemark getPlacemark() {
        return placemark;
    }

    public void setPlacemark(Placemark placemark) {
        this.placemark = placemark;
    }
}
