package com.decathlon.beacon.domain.oneid.access_token;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class OneIdTokenResponse {

    private String access_token,token_type;
    private Long expires_in;
}
