package com.decathlon.beacon.domain.oneid.constants;

/**
 * Created By Subrahmanya
 */

public class OneIdConstants {
  public static final String ACCESS_TOKEN_USERNAME_KEY = "username";
  public static final String ACCESS_TOKEN_USERNAME_VALUE = "evamall_oauth";
  public static final String ACCESS_TOKEN_GRANT_TYPE_KEY = "grant_type";
  public static final String ACCESS_TOKEN_GRANT_TYPE_VALUE = "password";
  public static final String ACCESS_TOKEN_PASSWORD_KEY = "password";
  public static final String ACCESS_TOKEN_PASSWORD_VALUE = "xEL2FjgpgE";
  public static final String ACCESS_TOKEN_SCOPE_KEY = "scope";
  public static final String ACCESS_TOKEN_SCOPE_VALUE = "openid profile";

  public static final String AUTHORIZATION = "Authorization";
  public static final String AUTH_TOKEN = "Basic ZXZhbWFsbF9hcHA6RUNhdjJXaVNTVm8wT0pwc0U2TFBVcVhqYjVyYW5MYU5pVlg5c0JIbkFpMDN6V2twVFNKeU1iU2Z6UGNrR2hOMA==";
  public static final String API_KEY_HEADER = "x-api-key";
  public static final String API_KEY = "6044aa4c-e90c-4653-84cf-211ba1de0e18";

  public static final String SEARCH_CUSTOMER_URL = "https://api-sg.decathlon.net/customers/sg/v2/customer_search/advanced_search?ppays=IN&state=7&client_id=open_bravo_in";
  public static final String CREATE_CUSTOMER_URL = "https://api-sg.decathlon.net/customers/sg/v1/customer_creation/smart_individual?ppays=IN&state=7&client_id=open_bravo_in";
  public static final String UPDATE_IDENTITY_URL = "https://api-sg.decathlon.net/customers/sg/v1/customer_data/identity_individual?ppays=IN&state=7&client_id=open_bravo_in&language=EN&signature=aa&id_person=";
  public static final String UPDATE_CONTACT_URL = "https://api-sg.decathlon.net/customers/sg/v1/customer_data/contact?ppays=IN&state=7&client_id=open_bravo_in&language=EN&signature=aa&id_person=";
  public static final String UPDATE_ADDRESS_URL = "https://api-sg.decathlon.net/customers/sg/v1/customer_data/billing_address?ppays=IN&state=7&client_id=open_bravo_in&language=EN&signature=aa&id_person=";

  public static final String SUCCESS = "Success";
  public static final String FAIL = "Fail";
  public static final Long RADIUS = 40L;
}