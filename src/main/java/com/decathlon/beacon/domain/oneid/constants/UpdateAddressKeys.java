package com.decathlon.beacon.domain.oneid.constants;

/**
 * Created By Subrahmanya
 */

public enum UpdateAddressKeys {
    ADDITIONAL_NUMBER("additional_number"),
    ADDITIONAL_DATA("additionnal_data"),
    BLOCK("block"),
    BUILDING("building"),
    CITY("city"),
    CITY_GEO_ID("city_geo_id"),
    COUNTRY_CODE("country_code"),
    COUNTRY_CODE_GEO_ID("country_code_geo_id"),
    DISTRICT("district"),
    DISTRICT_GEO_ID("district_geo_id"),
    FLAT("flat"),
    HOUSE("house"),
    LINE1("line1"),
    LINE2("line2"),
    POSTAL_CODE("postal_code"),
    PROVINCE_GEO_ID("province_geo_id"),
    POSTAL_CODE_GEO_ID("postal_code_geo_id"),
    PROVINCE("province"),
    STREET_NAME("street_name"),
    STREET_NUMBER("street_number");

    private String keys;

    UpdateAddressKeys(String keys) {
        this.keys = keys;
    }

    public String keys() {
        return keys;
    }
}
