package com.decathlon.beacon.domain.oneid.constants;

public enum UpdateContact {
    EMAIL("email"),
    EMAIL_VALID("email_valid"),
    MOBILE("mobile"),
    MOBILE_VALID("mobile_valid"),
    MOBILE_COUNTRY_CODE("mobile_country_code");

    private String key;

    UpdateContact(String key){
        this.key = key;
    }

    public String getKey(){
        return this.key;
    }
}
