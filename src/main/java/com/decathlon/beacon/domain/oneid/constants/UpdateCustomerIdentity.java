package com.decathlon.beacon.domain.oneid.constants;

/**
 * Created By Subrahmanya
 */

public enum UpdateCustomerIdentity {

    BIRTHDATE("birthdate"),
    COUNTRY_CODE("country_code"),
    LANGUAGE("language_code"),
    NAME("name"),
    NAME2("name2"),
    NATIONAL_ID("national_id"),
    NATIONAL_ID_TYPE("national_id_type"),
    PREFIX("prefix"),
    SURNAME("surname"),
    ID_PERSON("id_person"),
    MEMBER_TYPE("member_type"),
    SEX("sexe");

    private String key;

    UpdateCustomerIdentity(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
