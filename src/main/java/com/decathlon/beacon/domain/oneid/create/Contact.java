package com.decathlon.beacon.domain.oneid.create;

/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class Contact {
    	private Boolean mobile_valid,email_valid;
    	private String mobile,mobile_country_code,email;
}
