package com.decathlon.beacon.domain.oneid.create;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class CreateCustomer {

    private boolean abo_fid;
    private String 	billing_address;
    private Stores stores;
    private Contact contact;
    private Optin optin;
    private IndividualIdentity identity_individual;

}
