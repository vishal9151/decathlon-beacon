package com.decathlon.beacon.domain.oneid.create;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class IndividualIdentity {

   private Long member_type;
   private String national_id,country_code,language_code,birthdate, id_person, prefix, surname, name, sexe, name2, national_id_type;
}
