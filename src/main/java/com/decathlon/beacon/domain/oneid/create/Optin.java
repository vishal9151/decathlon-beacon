package com.decathlon.beacon.domain.oneid.create;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class Optin {
    	private Boolean partner, decathlon;
}
