package com.decathlon.beacon.domain.oneid.create;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class SearchCustomer {

    private String mobile,card_number,email;
}
