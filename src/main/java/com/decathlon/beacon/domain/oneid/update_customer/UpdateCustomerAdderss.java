package com.decathlon.beacon.domain.oneid.update_customer;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class UpdateCustomerAdderss {

    private String additional_number,additional_postal_code,additionnal_data,block,building,city, city_geo_id,country_code,country_code_geo_id, district,district_geo_id;
    private String flat,house,landline,landline_country_code,line1,line2,mobile,mobile_country_code,name,name2,national_id,national_id_type;
    private String postal_code,province_code,province_geo_id,street_name,street_number,surname,title,postal_code_geo_id,province;
    private Long id_address,id_person;
    private Boolean value,is_prefered;
}
