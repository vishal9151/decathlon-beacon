package com.decathlon.beacon.domain.oneid.update_customer;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class UpdateCustomerContact {
    private String email,mobile,mobile_country_code;
    private boolean email_valid,mobile_valid;
    private Long id_person;
}
