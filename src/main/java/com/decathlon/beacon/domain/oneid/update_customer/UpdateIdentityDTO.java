package com.decathlon.beacon.domain.oneid.update_customer;
/**
 * Created By Subrahmanya
 */

import lombok.Data;

@Data
public class UpdateIdentityDTO {

     private String birthdate,country_code,language_code,name,name2,national_id,national_id_type,prefix,surname;
     private Long id_person,member_type,sexe;
}
