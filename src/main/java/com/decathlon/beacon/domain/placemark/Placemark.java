package com.decathlon.beacon.domain.placemark;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by iglulabs on 31/1/18.
 */

@Entity
@Table(name = "placemark")
@SuppressWarnings("serial")
public class Placemark {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id", nullable = false)
    private Long id;

    @Column(name = "placemark_id")
    private String placemarkId;

    @Column(name = "shelf_num")
    private Long shelfNum;

    @Column(name = "placemark_name")
    private String placemarkName;

    @Column(name = "updated_on")
    private Timestamp updatedOn;

    @Column(name = "created_on")
    private Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlacemarkId() {
        return placemarkId;
    }

    public void setPlacemarkId(String placemarkId) {
        this.placemarkId = placemarkId;
    }

    public Long getShelfNum() {
        return shelfNum;
    }

    public void setShelfNum(Long shelfNum) {
        this.shelfNum = shelfNum;
    }

    public String getPlacemarkName() {
        return placemarkName;
    }

    public void setPlacemarkName(String placemarkName) {
        this.placemarkName = placemarkName;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * Sets createdAt before insert
     */
    @PrePersist
    public void setCreationDate() {
        this.createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }

    /**
     * Sets updatedAt before update
     */
    @PreUpdate
    public void setChangeDate() {
        this.updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
    }
}
