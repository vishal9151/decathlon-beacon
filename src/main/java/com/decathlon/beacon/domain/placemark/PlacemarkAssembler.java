package com.decathlon.beacon.domain.placemark;

import java.sql.Timestamp;

public class PlacemarkAssembler {
    private Long id;

    private String placemarkId;

    private int shelfNum;

    private String placemarkName;

    private Timestamp updatedOn;

    private Timestamp createdOn;

    public static PlacemarkDTO getPlacemarkDTOFromPlacemark(Placemark placemark){
        if(placemark!=null){
            final PlacemarkDTO placemarkDTO=new PlacemarkDTO();
            placemarkDTO.setId(placemark.getId());
            placemarkDTO.setPlacemarkId(placemark.getPlacemarkId());
            placemarkDTO.setShelfNum(placemark.getShelfNum());
            placemarkDTO.setPlacemarkName(placemark.getPlacemarkName());
            placemarkDTO.setUpdatedOn(placemark.getUpdatedOn());
            placemarkDTO.setCreatedOn(placemark.getCreatedOn());
            return placemarkDTO;
        }
        return null;
    }


}
