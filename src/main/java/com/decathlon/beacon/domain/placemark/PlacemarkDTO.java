package com.decathlon.beacon.domain.placemark;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

public class PlacemarkDTO {
    private Long id;

    private String placemarkId;

    private Long shelfNum;

    private String placemarkName;

    private Timestamp updatedOn;

    private Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlacemarkId() {
        return placemarkId;
    }

    public void setPlacemarkId(String placemarkId) {
        this.placemarkId = placemarkId;
    }

    public Long getShelfNum() {
        return shelfNum;
    }

    public void setShelfNum(Long shelfNum) {
        this.shelfNum = shelfNum;
    }

    public String getPlacemarkName() {
        return placemarkName;
    }

    public void setPlacemarkName(String placemarkName) {
        this.placemarkName = placemarkName;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }
}
