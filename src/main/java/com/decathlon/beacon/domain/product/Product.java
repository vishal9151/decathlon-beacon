package com.decathlon.beacon.domain.product;

import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.category.Category;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;

import static javax.persistence.FetchType.LAZY;

/**
 * Created by VISHAL on 26-12-2017.
 */

@Entity
@Table(name = "product")
public class Product implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "pixl_id")
    private String pixlID;

    @Column(name = "size")
    private String size;

    @Column(name = "ean")
    private String ean;

    @Column(name = "in_range")
    boolean inRange;

    @Column(name = "price")
    private Double price;

    @Column(name = "brand_id")
    private Long brandId;

    @Column(name = "brand_name")
    private String brandName;

    @Lob @Basic(fetch=LAZY)
    @Column(name="review")
    private String review;


    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "item_code")
    private String itemCode;

    @Column(name = "category_id")
    private Long categoryID;

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private Category category;

    @Column(name = "aruba_placemark_id")
    private Long arubaPlacemarkId;

    @ManyToOne()
    @JoinColumn(name = "aruba_placemark_id", insertable = false, updatable = false)
    private Placemark placemark;

    @Column(name = "status",length = 2)
    private int status;

    @Column(name = "updated_on")
    private Timestamp updatedOn;

    @Column(name = "created_on")
    private Timestamp createdOn;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        try {
            this.id = id;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        try{
            this.brandName = brandName;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        try{
            this.category = category;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        try{
            this.brandId = brandId;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        try{
            this.modelCode = modelCode;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        try{
            this.itemCode = itemCode;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Long getArubaPlacemarkId() {
        return arubaPlacemarkId;
    }

    public void setArubaPlacemarkId(Long arubaPlacemarkId) {
        this.arubaPlacemarkId = arubaPlacemarkId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        try {
            this.name = name;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        try{
            this.imageUrl = imageUrl;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getPixlID() {
        return pixlID;
    }

    public void setPixlID(String pixlID) {
        this.pixlID = pixlID;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public boolean isInRange() {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        try {
            this.updatedOn = updatedOn;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        try{
            this.createdOn = createdOn;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Placemark getPlacemark() {
        return placemark;
    }

    public void setPlacemark(Placemark placemark) {
        try{
            this.placemark = placemark;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        try{
            this.status = status;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    /**
     * Sets createdAt before insert
     */
    @PrePersist
    public void setCreationDate() {
        try {
            this.createdOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Sets updatedAt before update
     */
    @PreUpdate
    public void setChangeDate() {
        try {
            this.updatedOn = new Timestamp(Calendar.getInstance().getTimeInMillis());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
