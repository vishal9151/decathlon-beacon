package com.decathlon.beacon.domain.product;

import com.decathlon.beacon.domain.category.CategoryAssembler;
import com.decathlon.beacon.domain.placemark.PlacemarkAssembler;

public class ProductAssembler {

    public static ProductDTO getProductDTOFromProduct(Product product){
        try {
            if (product != null) {
                final ProductDTO productDTO = new ProductDTO();
                productDTO.setId(product.getId());
                productDTO.setName(product.getName());
                productDTO.setImageUrl(product.getImageUrl());
                productDTO.setPixlID(product.getPixlID());
                productDTO.setSize(product.getSize());
                productDTO.setEan(product.getEan());
                productDTO.setInRange(product.isInRange());
                productDTO.setPrice(product.getPrice());
                productDTO.setBrandId(product.getBrandId());
                productDTO.setBrandName(product.getBrandName());
                productDTO.setReview(product.getReview());
                productDTO.setModelCode(product.getModelCode());
                productDTO.setItemCode(product.getItemCode());
                productDTO.setCategoryID(product.getCategoryID());
                /*if (product.getCategory() != null)
                    productDTO.setCategoryDTO(CategoryAssembler.getCategoryDTOFromCategory(product.getCategory()));*/
                productDTO.setArubaPlacemarkId(product.getArubaPlacemarkId());
                if (product.getPlacemark() != null)
                    productDTO.setPlacemarkDTO(PlacemarkAssembler.getPlacemarkDTOFromPlacemark(product.getPlacemark()));
                productDTO.setStatus(product.getStatus());
                productDTO.setCreatedOn(product.getCreatedOn());
                productDTO.setUpdatedOn(product.getUpdatedOn());
                return productDTO;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
