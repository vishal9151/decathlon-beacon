package com.decathlon.beacon.domain.product;

import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.category.CategoryDTO;
import com.decathlon.beacon.domain.placemark.PlacemarkDTO;

import java.sql.Timestamp;

public class ProductDTO {
    private Long id;

    private String name;

    private String imageUrl;

    private String pixlID;

    private String size;

    private String ean;

    boolean inRange;

    private Double price;

    private Long brandId;

    private String brandName;

    private String review;

    private String modelCode;

    private String itemCode;

    private Long categoryID;

    private Long arubaPlacemarkId;

    private int status;

    private PlacemarkDTO placemarkDTO;

    /*We cannot use it as it will raise circular loop while converting Product to ProductDTO in its assembler*/
    /*private CategoryDTO categoryDTO;*/

    private Timestamp updatedOn;

    private Timestamp createdOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPixlID() {
        return pixlID;
    }

    public void setPixlID(String pixlID) {
        this.pixlID = pixlID;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public boolean isInRange() {
        return inRange;
    }

    public void setInRange(boolean inRange) {
        this.inRange = inRange;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getBrandId() {
        return brandId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Long categoryID) {
        this.categoryID = categoryID;
    }

    public Long getArubaPlacemarkId() {
        return arubaPlacemarkId;
    }

    public void setArubaPlacemarkId(Long arubaPlacemarkId) {
        this.arubaPlacemarkId = arubaPlacemarkId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public PlacemarkDTO getPlacemarkDTO() {
        return placemarkDTO;
    }

    public void setPlacemarkDTO(PlacemarkDTO placemarkDTO) {
        this.placemarkDTO = placemarkDTO;
    }

    /*public CategoryDTO getCategoryDTO() {
        return categoryDTO;
    }

    public void setCategoryDTO(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }
*/
    public Timestamp getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Timestamp updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }


}
