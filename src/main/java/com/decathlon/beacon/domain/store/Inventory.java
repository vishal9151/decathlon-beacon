package com.decathlon.beacon.domain.store;

import com.decathlon.beacon.domain.BaseDomain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "inventory")
public class Inventory extends BaseDomain implements Serializable{

    @Column(name = "model_code")
    private String modelCode;

    @Column(name = "placemark_id", columnDefinition="varchar(255)")
    private String placemarkID;

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getPlacemarkID() {
        return placemarkID;
    }

    public void setPlacemarkID(String placemarkID) {
        this.placemarkID = placemarkID;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                "modelCode='" + modelCode + '\'' +
                ", placemarkID=" + placemarkID +
                '}';
    }
}
