package com.decathlon.beacon.domain.store;

public class InventoryAssembler {

    public static InventoryDTO getInventoryDTOFromInventory(Inventory inventory){
        try {
            if (inventory != null) {
                InventoryDTO inventoryDTO=new InventoryDTO();
                inventoryDTO.setId(inventory.getId());
                inventoryDTO.setModelCode(inventory.getModelCode());
                inventoryDTO.setPlacemarkID(inventory.getPlacemarkID());
                return inventoryDTO;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
