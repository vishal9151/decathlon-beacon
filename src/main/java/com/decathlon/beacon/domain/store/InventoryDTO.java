package com.decathlon.beacon.domain.store;

public class InventoryDTO {

    private Long id;

    private String modelCode;

    private String placemarkID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getPlacemarkID() {
        return placemarkID;
    }

    public void setPlacemarkID(String placemarkID) {
        this.placemarkID = placemarkID;
    }
}
