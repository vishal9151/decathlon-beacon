package com.decathlon.beacon.models;

import com.decathlon.beacon.domain.oneid.create.Contact;
import com.decathlon.beacon.domain.oneid.create.IndividualIdentity;
import com.decathlon.beacon.domain.oneid.create.Optin;
import com.decathlon.beacon.domain.oneid.create.Stores;
import lombok.Data;
@Data
public class CreateCustomerDTO {
    private String billing_address;
    private boolean abo_fid;
    private Stores stores;
    private Contact contact;
    private Optin optin;
    private IndividualIdentity identity_individual;

}
