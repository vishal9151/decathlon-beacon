package com.decathlon.beacon.models;

/**
 * Created by VISHAL on 16-02-2017.
 */
public class ExcelCell {
    private String cellName;
    private String tableColumn;
    private Class cellType;
    private Boolean required;

    public ExcelCell(String cellName) {
        this.cellName = cellName;
        //this.tableColumn = tableColumn;
        this.cellType = String.class;
        this.required = true;
    }

    public ExcelCell(String cellName, Boolean required) {
        this.cellName = cellName;
        //this.tableColumn = tableColumn;
        this.cellType = String.class;
        this.required = required;
    }

    public ExcelCell(String cellName, Class cellType) {
        this.cellName = cellName;
        //this.tableColumn = tableColumn;
        this.cellType = cellType;
        this.required = true;
    }

    public ExcelCell(String cellName, String tableColumn) {
        this.cellName = cellName;
        this.tableColumn = tableColumn;
        this.cellType = String.class;
        this.required = true;
    }

    public ExcelCell(String cellName, String tableColumn, Class cellType) {
        this.cellName = cellName;
        this.tableColumn = tableColumn;
        this.cellType = cellType;
        this.required = true;
    }

    public ExcelCell(String cellName, String tableColumn, Boolean required) {
        this.cellName = cellName;
        this.tableColumn = tableColumn;
        this.required = required;
        this.cellType = String.class;
    }

    public ExcelCell(String cellName, String tableColumn, Class cellType, Boolean required) {
        this.cellName = cellName;
        this.tableColumn = tableColumn;
        this.cellType = cellType;
        this.required = required;
    }

    public String getCellName() {
        return cellName;
    }

    public void setCellName(String cellName) {
        this.cellName = cellName;
    }

    public Class getCellType() {
        return cellType;
    }

    public void setCellType(Class cellType) {
        this.cellType = cellType;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getTableColumn() {
        return tableColumn;
    }

    public void setTableColumn(String tableColumn) {
        this.tableColumn = tableColumn;
    }
}
