package com.decathlon.beacon.models;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Line {
    @JsonCreator
    public Line(){

    }

    private String itemCode;
    private long qty;
    private String type;
    private double unitPrice;

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
}
