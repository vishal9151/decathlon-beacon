package com.decathlon.beacon.models;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class OrderDetails {
    private boolean PaymentDone;
    //@SerializedName("address")
    @JsonProperty("address")
    private ArrayList<Address> addresses;
    private String email;
    private int Feedback;
    //@SerializedName("lines")
    @JsonProperty("lines")
    private ArrayList<Line> lines;
    private String mobile;
    private String MyDecathlonId;
    private String name;
    private String orderDate;
    private String orderNumber;
    private String paymentMethod;
    private String salebegintime;/**/
    private String saleendtime;/*format of time will be */
    private String store;
    private double totalGrossAmount;
    private String billID;

    public boolean isPaymentDone() {
        return PaymentDone;
    }

    public void setPaymentDone(boolean paymentDone) {
        PaymentDone = paymentDone;
    }

    public ArrayList<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<Address> addresses) {
        this.addresses = addresses;
    }

    public ArrayList<Line> getLines() {
        return lines;
    }

    public void setLines(ArrayList<Line> lines) {
        this.lines = lines;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getFeedback() {
        return Feedback;
    }

    public void setFeedback(int feedback) {
        Feedback = feedback;
    }


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMyDecathlonId() {
        return MyDecathlonId;
    }

    public void setMyDecathlonId(String myDecathlonId) {
        MyDecathlonId = myDecathlonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSalebegintime() {
        return salebegintime;
    }

    public void setSalebegintime(String salebegintime) {
        this.salebegintime = salebegintime;
    }

    public String getSaleendtime() {
        return saleendtime;
    }

    public void setSaleendtime(String saleendtime) {
        this.saleendtime = saleendtime;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public double getTotalGrossAmount() {
        return totalGrossAmount;
    }

    public void setTotalGrossAmount(double totalGrossAmount) {
        this.totalGrossAmount = totalGrossAmount;
    }

    public String getBillID() {
        return billID;
    }

    public void setBillID(String billID) {
        this.billID = billID;
    }





}
