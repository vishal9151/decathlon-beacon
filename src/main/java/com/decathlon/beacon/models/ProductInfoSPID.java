package com.decathlon.beacon.models;

import com.google.gson.annotations.SerializedName;

public class ProductInfoSPID {
    private long modelSeasonId;
    private long modelSeasonLocalizedId;
    private String alfrescoId;
    private String locale;
    private String modelInfoName;
    private String webLabel;
    @SerializedName("productNature")
    private ProductNature productNature;
    private String productSellingStartWeek;
    private String productSellingEndWeek;
    private boolean published;
    private long lastPublishedDate;
    private boolean reconductionManually;
    private boolean saveManually;
    @SerializedName("season")
    private Season season;
    private String status;
    @SerializedName("brand")
    private Brand brand;
    private String code;/*code can be used as model code*/
    private Family family;
    private Department department;
    private Universe universe;
    private SubDepartment subDepartment;
    private long familyCode;
    private String familyName;
    private FirstPackShotWeb firstPackShotWeb;

    /*NOTE: There are lot of other attributes coming in this object but as
    * we don't have usage confirmation, we have not parsed all of them. This object in detail can be seen
     * under resources*/

    public long getModelSeasonId() {
        return modelSeasonId;
    }

    public void setModelSeasonId(long modelSeasonId) {
        this.modelSeasonId = modelSeasonId;
    }

    public long getModelSeasonLocalizedId() {
        return modelSeasonLocalizedId;
    }

    public void setModelSeasonLocalizedId(long modelSeasonLocalizedId) {
        this.modelSeasonLocalizedId = modelSeasonLocalizedId;
    }

    public String getAlfrescoId() {
        return alfrescoId;
    }

    public void setAlfrescoId(String alfrescoId) {
        this.alfrescoId = alfrescoId;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getModelInfoName() {
        return modelInfoName;
    }

    public void setModelInfoName(String modelInfoName) {
        this.modelInfoName = modelInfoName;
    }

    public String getWebLabel() {
        return webLabel;
    }

    public void setWebLabel(String webLabel) {
        this.webLabel = webLabel;
    }

    public ProductNature getProductNature() {
        return productNature;
    }

    public void setProductNature(ProductNature productNature) {
        this.productNature = productNature;
    }

    public String getProductSellingStartWeek() {
        return productSellingStartWeek;
    }

    public void setProductSellingStartWeek(String productSellingStartWeek) {
        this.productSellingStartWeek = productSellingStartWeek;
    }

    public String getProductSellingEndWeek() {
        return productSellingEndWeek;
    }

    public void setProductSellingEndWeek(String productSellingEndWeek) {
        this.productSellingEndWeek = productSellingEndWeek;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public long getLastPublishedDate() {
        return lastPublishedDate;
    }

    public void setLastPublishedDate(long lastPublishedDate) {
        this.lastPublishedDate = lastPublishedDate;
    }

    public boolean isReconductionManually() {
        return reconductionManually;
    }

    public void setReconductionManually(boolean reconductionManually) {
        this.reconductionManually = reconductionManually;
    }

    public boolean isSaveManually() {
        return saveManually;
    }

    public void setSaveManually(boolean saveManually) {
        this.saveManually = saveManually;
    }

    public Season getSeason() {
        return season;
    }

    public void setSeason(Season season) {
        this.season = season;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Universe getUniverse() {
        return universe;
    }

    public void setUniverse(Universe universe) {
        this.universe = universe;
    }

    public SubDepartment getSubDepartment() {
        return subDepartment;
    }

    public void setSubDepartment(SubDepartment subDepartment) {
        this.subDepartment = subDepartment;
    }

    public long getFamilyCode() {
        return familyCode;
    }

    public void setFamilyCode(long familyCode) {
        this.familyCode = familyCode;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public FirstPackShotWeb getFirstPackShotWeb() {
        return firstPackShotWeb;
    }

    public void setFirstPackShotWeb(FirstPackShotWeb firstPackShotWeb) {
        this.firstPackShotWeb = firstPackShotWeb;
    }

    public class FirstPackShotWeb{
        private long displayOrder;
        private String modelCode;
        private String zone;
        private Packshot packshot;

        public long getDisplayOrder() {
            return displayOrder;
        }

        public void setDisplayOrder(long displayOrder) {
            this.displayOrder = displayOrder;
        }

        public String getModelCode() {
            return modelCode;
        }

        public void setModelCode(String modelCode) {
            this.modelCode = modelCode;
        }

        public String getZone() {
            return zone;
        }

        public void setZone(String zone) {
            this.zone = zone;
        }

        public Packshot getPackshot() {
            return packshot;
        }

        public void setPackshot(Packshot packshot) {
            this.packshot = packshot;
        }

        public class Packshot{
            private long id;
            private String pixlId;
            private long validateFromDate;
            private long validateToDate;
            private String name;

            public long getId() {
                return id;
            }

            public void setId(long id) {
                this.id = id;
            }

            public String getPixlId() {
                return pixlId;
            }

            public void setPixlId(String pixlId) {
                this.pixlId = pixlId;
            }

            public long getValidateFromDate() {
                return validateFromDate;
            }

            public void setValidateFromDate(long validateFromDate) {
                this.validateFromDate = validateFromDate;
            }

            public long getValidateToDate() {
                return validateToDate;
            }

            public void setValidateToDate(long validateToDate) {
                this.validateToDate = validateToDate;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
    public class SubDepartment{
        private long code;
        private String name;

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public class Universe{
        private long code;
        private String name;

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Department{
        private long code;
        private String name;

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public class Family{
        private long code;
        private String name;

        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public class Brand{
        private long id;
        private String name;
        private boolean passion;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isPassion() {
            return passion;
        }

        public void setPassion(boolean passion) {
            this.passion = passion;
        }
    }
    public class Season{
        private int year;
        private String type;
        private String startWeek;
        private String seasonName;
        private String endWeek;

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStartWeek() {
            return startWeek;
        }

        public void setStartWeek(String startWeek) {
            this.startWeek = startWeek;
        }

        public String getSeasonName() {
            return seasonName;
        }

        public void setSeasonName(String seasonName) {
            this.seasonName = seasonName;
        }

        public String getEndWeek() {
            return endWeek;
        }

        public void setEndWeek(String endWeek) {
            this.endWeek = endWeek;
        }
    }
    public class ProductNature{
        long id;
        String label;
        String locale;
        boolean isActive;
        long level;
        long fatherId;
        long fatherLevel;
        String type;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getLocale() {
            return locale;
        }

        public void setLocale(String locale) {
            this.locale = locale;
        }

        public boolean isActive() {
            return isActive;
        }

        public void setActive(boolean active) {
            isActive = active;
        }

        public long getLevel() {
            return level;
        }

        public void setLevel(long level) {
            this.level = level;
        }

        public long getFatherId() {
            return fatherId;
        }

        public void setFatherId(long fatherId) {
            this.fatherId = fatherId;
        }

        public long getFatherLevel() {
            return fatherLevel;
        }

        public void setFatherLevel(long fatherLevel) {
            this.fatherLevel = fatherLevel;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
