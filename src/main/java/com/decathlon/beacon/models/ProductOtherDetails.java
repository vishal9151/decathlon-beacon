package com.decathlon.beacon.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/*{
        "model_id": "8331854",
        "brand": "CAMP",
        "name": "BOTTLE 0,8L PP PURPLE",
        "items": [
        {
        "id": "642272",
        "ean": "642272",
        "size": "0.8L",
        "sale_price": 99,
        "in_range": true
        }
        ]
        }*/
public class ProductOtherDetails {
    @SerializedName("model_id")
    private String modelID;
    private String brand;
    private String name;
    private ArrayList<Item> items;

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public class Item{
        private String id;
        private String ean;
        private String size;
        @SerializedName("sale_price")
        private String salePrice;
        @SerializedName("in_range")
        private boolean inRange;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEan() {
            return ean;
        }

        public void setEan(String ean) {
            this.ean = ean;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(String salePrice) {
            this.salePrice = salePrice;
        }

        public boolean isInRange() {
            return inRange;
        }

        public void setInRange(boolean inRange) {
            this.inRange = inRange;
        }
    }

}
