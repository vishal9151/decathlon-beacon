package com.decathlon.beacon.models;

import org.springframework.http.HttpStatus;

/**
 * Created by VISHAL on 26-12-2017.
 */
public class Result {

    private String message;
    private boolean status;
    private Object resultObject;
    private HttpStatus statusCode;

    public Result() {
    }

    public Result(String message, boolean status) {
        this.message = message;
        this.status = status;
    }

    public Result(Object resultObject, boolean status) {
        this.resultObject = resultObject;
        this.status = status;
    }

    public Result(String message, boolean status, Object resultObject,HttpStatus statusCode) {
        this.message = message;
        this.status = status;
        this.resultObject = resultObject;
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }

    public HttpStatus getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(HttpStatus statusCode) {
        this.statusCode = statusCode;
    }
}
