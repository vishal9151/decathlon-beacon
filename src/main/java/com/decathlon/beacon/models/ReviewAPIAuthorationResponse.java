package com.decathlon.beacon.models;

public class ReviewAPIAuthorationResponse {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
