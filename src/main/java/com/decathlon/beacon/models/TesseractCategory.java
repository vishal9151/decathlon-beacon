package com.decathlon.beacon.models;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TesseractCategory {

    private String name;
    private String id;
    private String hierarchicalParentId;
    private ArrayList<String> itemIds;
    private ArrayList<String> childCategoryIds;
    private ArrayList<Filter> filters;
    private ArrayList<Translations> translations;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHierarchicalParentId() {
        return hierarchicalParentId;
    }

    public void setHierarchicalParentId(String hierarchicalParentId) {
        this.hierarchicalParentId = hierarchicalParentId;
    }

    public ArrayList<String> getItemIds() {
        return itemIds;
    }

    public void setItemIds(ArrayList<String> itemIds) {
        this.itemIds = itemIds;
    }

    public ArrayList<String> getChildCategoryIds() {
        return childCategoryIds;
    }

    public void setChildCategoryIds(ArrayList<String> childCategoryIds) {
        this.childCategoryIds = childCategoryIds;
    }

    public ArrayList<Filter> getFilters() {
        return filters;
    }

    public void setFilters(ArrayList<Filter> filters) {
        this.filters = filters;
    }

    public ArrayList<Translations> getTranslations() {
        return translations;
    }

    public void setTranslations(ArrayList<Translations> translations) {
        this.translations = translations;
    }

    public class Filter{
        private String filterType;
        private String value;
        private String sortType;

        public String getFilterType() {
            return filterType;
        }

        public void setFilterType(String filterType) {
            this.filterType = filterType;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getSortType() {
            return sortType;
        }

        public void setSortType(String sortType) {
            this.sortType = sortType;
        }
    }

    public class Translations{
        private String language;
        private String label;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }
    }

}
