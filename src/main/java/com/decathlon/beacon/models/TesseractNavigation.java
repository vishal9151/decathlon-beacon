package com.decathlon.beacon.models;

import com.google.gson.annotations.SerializedName;

public class TesseractNavigation {

    private String sportCode;
    @SerializedName("metadata")
    private MetaData metaData;

    public String getSportCode() {
        return sportCode;
    }

    public void setSportCode(String sportCode) {
        this.sportCode = sportCode;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public class MetaData{
        private String geographicEntity;
        private String categoryId;
        private String thumbnailId;
        private String mobileBackgroundId;
        private String tabletBackgroundId;
        private String desktopBackgroundId;

        public String getGeographicEntity() {
            return geographicEntity;
        }

        public void setGeographicEntity(String geographicEntity) {
            this.geographicEntity = geographicEntity;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getThumbnailId() {
            return thumbnailId;
        }

        public void setThumbnailId(String thumbnailId) {
            this.thumbnailId = thumbnailId;
        }

        public String getMobileBackgroundId() {
            return mobileBackgroundId;
        }

        public void setMobileBackgroundId(String mobileBackgroundId) {
            this.mobileBackgroundId = mobileBackgroundId;
        }

        public String getTabletBackgroundId() {
            return tabletBackgroundId;
        }

        public void setTabletBackgroundId(String tabletBackgroundId) {
            this.tabletBackgroundId = tabletBackgroundId;
        }

        public String getDesktopBackgroundId() {
            return desktopBackgroundId;
        }

        public void setDesktopBackgroundId(String desktopBackgroundId) {
            this.desktopBackgroundId = desktopBackgroundId;
        }
    }

}
