package com.decathlon.beacon.repository;


import com.decathlon.beacon.domain.category.Category;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends BaseRepository<Category,Long> {

    Optional<Category> findByName(String name);

    Category findById(int id);

    List<Category> findAll();

    Category findCategoryByName(String name);

    Category findCategoryByCategoryID(String categoryID);

}
