package com.decathlon.beacon.repository;


import com.decathlon.beacon.domain.store.Inventory;

public interface InventoryRepository extends BaseRepository<Inventory,Long>{

    Inventory findInventoryByModelCode(String modelCode);

}
