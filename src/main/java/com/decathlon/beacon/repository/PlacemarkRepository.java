package com.decathlon.beacon.repository;

import com.decathlon.beacon.domain.category.Category;
import com.decathlon.beacon.domain.placemark.Placemark;

import java.util.List;

public interface PlacemarkRepository extends BaseRepository<Placemark,Long> {

    Placemark findPlacemarkByPlacemarkId(String placemarkID);

    List<Placemark> findPlacemarksByPlacemarkId(String placemarkID);
}
