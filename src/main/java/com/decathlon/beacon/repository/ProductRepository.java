package com.decathlon.beacon.repository;

import com.decathlon.beacon.domain.product.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductRepository extends BaseRepository<Product,Long> {

    @Override
    List<Product> findAll();

    Product findProductByModelCode(String modelCode);

    Product findProductByItemCode(String itemCode);

    List<Product> findProductsByModelCode(String modelCode);

    Product findProductByModelCodeAndItemCode(String modelCode,String itemCode);

    @Query(value = "select p.* from product p \n" +
            "join category c on c.id=p.category_id\n" +
            "where lower(c.name) like lower(concat('%',:searchParam , '%')) or\n" +
            "lower(p.name) like lower(concat('%',:searchParam, '%')) or\n" +
            "lower(p.brand_name) like lower(concat('%',:searchParam, '%')) limit 200;",
            nativeQuery = true)
    List<Product> searchProductInCategoryAndProduct(@Param("searchParam") String searchParam);


}
