package com.decathlon.beacon.repository;

import com.decathlon.beacon.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends BaseRepository<User,Long>{

    User findUserByMobile(String mobileNumber);

    @Query("SELECT t FROM User t WHERE " +
    "t.mobile LIKE :mobileNumber AND " +
    "t.otp LIKE :OTP")
    User findUserByMobileNumberAndOTP(@Param("mobileNumber") String mobileNumber,
                                      @Param("OTP") int OTP);

}
