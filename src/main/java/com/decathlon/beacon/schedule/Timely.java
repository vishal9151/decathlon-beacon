package com.decathlon.beacon.schedule;

import com.decathlon.beacon.services.CommonService;
import com.decathlon.beacon.util.Constants;
import com.decathlon.beacon.util.CategoryAndProductModifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/*
----cron pattern : The pattern is a list of six single
space-separated fields: representing second, minute, hour, day, month, weekday.
Month and weekday names can be given as the first three letters of the English names.

For ex:

        "0 0 * * * *" = the top of every hour of every day.
        "10 * * * * *" = every ten seconds.
        "0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
        "0 0 6,19 * * *" = 6:00 AM and 7:00 PM every day.
        "0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30, 10:00 and 10:30 every day.
        "0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
        "0 0 0 25 12 ?" = every Christmas Day at midnight
*/

@Component
public class Timely {
    private static final Logger log = LoggerFactory.getLogger(Timely.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    int cronInvokeCounter;

    @Autowired
    CategoryAndProductModifier categoryAndProductModifier;

    //"00 05 19 23 04 ?"

    @Scheduled(cron = "1 1 1 1 * *")
    public void cronToUpdateCategoriesAndProducts() {
        try {
            ++cronInvokeCounter;
            log.info("cronToUpdateCategoriesAndProducts invoke at : "+ Calendar.getInstance().getTimeInMillis());
            categoryAndProductModifier.modifyCategoryAndProductDetails();
        }catch (Exception e){
            log.error(Constants.ERROR_WHILE_RUNNING_CRON_FOR_CATEGORY_UPDATE+ " at: "+
                    Calendar.getInstance().getTimeInMillis());
            e.printStackTrace();
        }
    }
}


