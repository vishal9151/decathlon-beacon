package com.decathlon.beacon.services;

import com.decathlon.beacon.domain.category.Category;
import com.decathlon.beacon.domain.oneid.access_token.OneIdTokenResponse;
import com.decathlon.beacon.domain.oneid.create.CreateCustomer;
import com.decathlon.beacon.domain.oneid.search_customer.SearchCustomerDTO;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateCustomerContact;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateIdentityDTO;
import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.models.OrderDetails;
import com.decathlon.beacon.models.Result;

import java.util.List;
import java.util.Map;

/**
 * Created by VISHAL on 26-12-2017.
 */

public interface CommonService {

    Result getAllCategories();

    Category getCategoryByName(String name);

    Category getCategoryByCategoryID(String categoryID);

    Result saveCategories();

    Category saveCategory(Category category);

    Product saveProduct(Product product);

    Result getSubCategoriesByCategory(int categoryId);

    Result getProductsBySubCategory(int subCategoryId);

    Result searchProducts(int currentPage, int pageSize, String searchString);

    List<Product> getAllProducts();

    Product getProductByModelCode(String modelCode);

    Product getProductByItemCode(String itemCode);

    Product getProductByModelCodeAndItemCode(String modelCode, String itemCode);

    Result getAllPlacemarks();

    Result updateProductPlacemarks(String modelCode, String placemarkId);

    boolean updateInventory(String modelCode, String placemarkID);

    Result sendSms(String mobileNumber);

    Result validateOtp(String mobile, int otp);

    Result sendOrderConfirmationSMS(OrderDetails orderDetails);

    void sendMessageAboutTesseractChildCategoryUpdate(String message);

    List<Product> searchProduct(String searchString);

    Result getPlacemarkByModelCode(String modelCode);

    boolean checkProductAvailability(Category category);

    OneIdTokenResponse getAccessToken();

    Result getCustomerDetails(SearchCustomerDTO customerDTO);

    Result createCustomer(CreateCustomer customer);

    Result updateCustomerIdentity(UpdateIdentityDTO dto);

    Result updateCustomerContact(UpdateCustomerContact customerContact);

    Result updateCustomerAddress(Map<String, Object> adderss);

    Boolean checkUser(SearchCustomerDTO dto);

    String getUsername();

    Long checkUser();
}
