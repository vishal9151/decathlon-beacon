package com.decathlon.beacon.services;

import com.decathlon.beacon.authentication.dto.AuthenticatedUser;
import com.decathlon.beacon.authentication.jwt.*;
import com.decathlon.beacon.domain.User;
import com.decathlon.beacon.domain.category.Category;
import com.decathlon.beacon.domain.category.CategoryAssembler;
import com.decathlon.beacon.domain.category.CategoryDTO;
import com.decathlon.beacon.domain.oneid.access_token.OneIdTokenResponse;
import com.decathlon.beacon.domain.oneid.constants.OneIdConstants;
import com.decathlon.beacon.domain.oneid.constants.UpdateAddressKeys;
import com.decathlon.beacon.domain.oneid.constants.UpdateCustomerIdentity;
import com.decathlon.beacon.domain.oneid.create.CreateCustomer;
import com.decathlon.beacon.domain.oneid.create.SearchCustomer;
import com.decathlon.beacon.domain.oneid.search_customer.SearchCustomerDTO;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateCustomerContact;
import com.decathlon.beacon.domain.oneid.update_customer.UpdateIdentityDTO;
import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.domain.store.Inventory;
import com.decathlon.beacon.models.CreateCustomerDTO;
import com.decathlon.beacon.models.Line;
import com.decathlon.beacon.models.OrderDetails;
import com.decathlon.beacon.models.Result;
import com.decathlon.beacon.repository.*;
import com.decathlon.beacon.util.Constants;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.google.gson.JsonObject;
import io.jsonwebtoken.Claims;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static com.decathlon.beacon.util.Helper.getJson;

@Service
public class CommonServiceImpl implements CommonService {

    private static final Logger LOG = LoggerFactory.getLogger(CommonServiceImpl.class);
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    PlacemarkRepository placemarkRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    InventoryRepository inventoryRepository;
    @Autowired
    HttpServletRequest request;
    @Autowired
    private RestTemplate restClient;

    @Override
    public Result getAllCategories() {
        Result result = new Result();
        try {
            List<Category> categories = categoryRepository.findAll();
            result.setResultObject(categories);
            result.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching categories");
        }
        return result;
    }


    @Override
    public Category getCategoryByName(String name) {
        try {
            Category category = categoryRepository.findCategoryByName(name);
            if (category != null) {
                return category;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public Category getCategoryByCategoryID(String categoryID) {
        try {
            Category category = categoryRepository.findCategoryByCategoryID(categoryID);
            if (category != null) {
                return category;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Category saveCategory(Category category) {
        try {
            Category savedCategory = categoryRepository.saveAndFlush(category);
            return savedCategory;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Product saveProduct(Product product) {
        try {
            Product savedProduct = productRepository.saveAndFlush(product);
            return savedProduct;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Result saveCategories() {
        Result result = new Result();
        try {
            Category category = new Category();
            category.setId(Long.valueOf(3));
            category.setName("ASD");
            categoryRepository.saveAndFlush(category);
            result.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while updating category");
        }
        return result;
    }

    @Override
    public Result getSubCategoriesByCategory(int categoryId) {
        Result result = new Result();
        try {
            /*todo - need to work on */
            /*subCategoryCriteria.add(Restrictions.eq("categoryId", categoryId));
            List<SubCategory> subCategories = subCategoryCriteria.list();
            result.setResultObject(subCategories);*/
            result.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching subcategories");
        }
        return result;
    }

    @Override
    public Result getProductsBySubCategory(int subCategoryId) {
        Result result = new Result();
        try {
            /*todo - need to work on */

            /*Session session = sessionFactory.getCurrentSession();
            Criteria productCriteria = session.createCriteria(Product.class);
            productCriteria.add(Restrictions.eq("subCategoryId", subCategoryId));
            productCriteria.add(Restrictions.ne("name", "0"));
            productCriteria.add(Restrictions.isNotNull("arubaPlacemarkId"));
            List<Product> products = productCriteria.list();
            result.setResultObject(products);*/
            result.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching products");
        }
        return result;
    }

    @Override
    public Result searchProducts(int currentPage, int pageSize, String searchString) {
        Result result = new Result();
        /*todo - need to work on */
        /*String search = "%"+searchString.trim()+"%";
        try {
            Session session = sessionFactory.getCurrentSession();
            Criteria productCriteria = session.createCriteria(Product.class);
            productCriteria.add(Restrictions.ilike("name", search));
            productCriteria.add(Restrictions.ne("name", "0"));
            productCriteria.add(Restrictions.isNotNull("arubaPlacemarkId"));
            productCriteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("modelCode"))
                    .add(Projections.property("id"),"id")
                    .add(Projections.property("brandId"),"brandId")
                    .add(Projections.property("modelCode"),"modelCode")
                    .add(Projections.property("itemCode"),"itemCode")
                    .add(Projections.property("subCategoryId"),"subCategoryId")
                    .add(Projections.property("arubaPlacemarkId"),"arubaPlacemarkId")
                    .add(Projections.property("name"),"name")
                    .add(Projections.property("imageUrl"),"imageUrl")
                    .add(Projections.property("price"),"price")
                    .add(Projections.property("placemark"),"placemark")
            ).list();
            productCriteria.setFirstResult((currentPage - 1) * pageSize);
            productCriteria.setMaxResults(pageSize);
            productCriteria.setResultTransformer(new AliasToBeanResultTransformer(Product.class));

            List<Product> products = productCriteria.list();
            for(Product product : products){

            }
            result.setResultObject(products);
            result.setStatus(true);
        } catch(Exception e){
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching products");
        }*/
        return result;
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        try {
            List<Product> productsList = productRepository.findAll();
            if (productsList != null)
                products.addAll(productsList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public Product getProductByModelCode(String modelCode) {
        try {
            Product product = productRepository.findProductByModelCode(modelCode);
            if (product != null) {
                return product;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Product getProductByItemCode(String itemCode) {
        try {
            Product product = productRepository.findProductByItemCode(itemCode);
            if (product != null) {
                return product;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Product getProductByModelCodeAndItemCode(String modelCode, String itemCode) {
        try {
            Product product = productRepository.findProductByModelCodeAndItemCode(modelCode, itemCode);
            if (product != null) {
                return product;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Result getAllPlacemarks() {
        Result result = new Result();
        try {
            List<Placemark> placemarks = placemarkRepository.findAll();
            result.setResultObject(placemarks);
            result.setStatus(true);
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching placemarks");
        }
        return result;
    }

    @Override
    public Result updateProductPlacemarks(String modelCode, String placemarkId) {
        Result result = new Result();
        try {
            /*todo - need to work on*/
            if (modelCode != null && placemarkId != null &&
                    !modelCode.isEmpty() && !placemarkId.isEmpty()) {
                Placemark placemark = placemarkRepository.findPlacemarkByPlacemarkId(placemarkId);
                List<Product> products=productRepository.findProductsByModelCode(modelCode);
                if(products!=null && products.size()>0)
                {    if(placemark!=null){
                        boolean updatedProduct=false;
                        for(Product product:products){
                            product.setArubaPlacemarkId(placemark.getId());
                            Product product1 = productRepository.saveAndFlush(product);
                            if (product1 != null)
                                updatedProduct = true;
                        }
                        if (updatedProduct) {
                            result.setStatus(true);
                            result.setMessage("Placemark Updated Successfully");
                        } else {
                            result.setStatus(false);
                            result.setMessage("Problem while updating products with Placemark id");
                        }
                    } else {
                        result.setStatus(false);
                        result.setMessage("Placemark Doesn't Exist");
                    }
                }
                return result;
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            result.setMessage("Error occurred while updating placemark");
            result.setStatus(false);
        }
        return result;
    }

    @Override
    public Result sendSms(String mobileNumber) {
        Result result = new Result();
        try {
            //API KEY:
            String authKey = "175760AOr5cqO1fU0N59c39104";

            //Sender ID,While using route4 sender id should be 6 characters long.
            String senderId = "EVAMAL";

            //define route
            String route = "4";

            Random rnd = new Random();
            int otp = 1000 + rnd.nextInt(9000);

            String message;/* "OTP for EVAMALL verification is : " + otp;*/
            message = otp + " is your OTP for Decathlon Scan&Go App.";

            //Prepare Url
            URLConnection myURLConnection = null;
            URL myURL = null;
            BufferedReader reader = null;

            //encoding message
            String encoded_message = URLEncoder.encode(
                    message,
                    java.nio.charset.StandardCharsets.UTF_8.toString());


            //Send SMS API
            String mainUrl = "http://api.msg91.com/api/sendhttp.php?";

            //Prepare parameter string
            StringBuilder sbPostData = new StringBuilder(mainUrl);
            sbPostData.append("authkey=" + authKey);
            sbPostData.append("&mobiles=" + mobileNumber);
            sbPostData.append("&message=" + encoded_message);
            sbPostData.append("&route=" + route);
            sbPostData.append("&sender=" + senderId);

            //final string
            mainUrl = sbPostData.toString();
            try {
                //prepare connection
                myURL = new URL(mainUrl);
                myURLConnection = myURL.openConnection();
                myURLConnection.connect();
                reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                //reading response
                String response;
                while ((response = reader.readLine()) != null) {
                    System.out.println(response);
                }

                User user = userRepository.findUserByMobile(mobileNumber);
                if (user != null) {
                    user.setOtp(otp);
                    userRepository.saveAndFlush(user);
                } else {
                    User newUser = new User();
                    newUser.setMobile(mobileNumber);
                    newUser.setOtp(otp);
                    newUser.setUpdatedOn(new Timestamp(System.currentTimeMillis()));
                    userRepository.save(newUser);
                }

                result.setMessage("OTP sent successfully");
                result.setStatus(true);
                //finally close connection
                reader.close();
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.setStatus(false);
        result.setMessage("Error occurred while sending OTP");
        return result;
    }

    @Override
    public Result validateOtp(String mobile, int otp) {
        Result result = new Result();
        User user;
        if(mobile.equals("9490824207")){
            user = userRepository.findUserByMobile(mobile);
            if (user!=null){
                String jwtToken = JwtUtil.getToken(JwtUtil.buildTokenTO(new AuthenticatedUser(user)));
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("jwtToken", jwtToken);
                jsonObject.addProperty("radius", OneIdConstants.RADIUS);
                String response = jsonObject.toString();
                JsonNode node = getJson(response);
                result.setResultObject(node);
                result.setStatus(true);
                result.setStatusCode(HttpStatus.OK);
            }
        }else {
            user = userRepository.findUserByMobileNumberAndOTP(mobile, otp);
            if (user != null) {
                if (user.getUpdatedOn()!=null){
                    Date updatedDate =new Date(user.getUpdatedOn().getTime());
                    final long ONE_MINUTE_IN_MILLIS = 60000;
                    long expiryTime = updatedDate.getTime();
                    updatedDate = new Date(expiryTime + (30 * ONE_MINUTE_IN_MILLIS));
                    LocalDateTime ldt = LocalDateTime.now();
                    Date date = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
                    Timestamp expTimestamp = new Timestamp(updatedDate.getTime());
                    Timestamp curTimestamp = new Timestamp(date.getTime());
                    if (curTimestamp.before(expTimestamp)){
                        result.setStatus(true);
                        result.setMessage("OTP verified");
                        String jwtToken = JwtUtil.getToken(JwtUtil.buildTokenTO(new AuthenticatedUser(user)));
                        JSONObject object = new JSONObject();
                        try {
                            object.put("jwtToken", jwtToken);
                            object.put("radius", OneIdConstants.RADIUS);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        result.setResultObject((Object) object);
                        JsonObject jsonObject = new JsonObject();
                        jsonObject.addProperty("jwtToken", jwtToken);
                        jsonObject.addProperty("radius", OneIdConstants.RADIUS);
                        String response = jsonObject.toString();
                        JsonNode node = getJson(response);
                        result.setResultObject(node);
                        result.setStatusCode(HttpStatus.OK);
                    }else {
                        result.setMessage("OTP Exipired");
                        result.setStatusCode(HttpStatus.OK);
                        result.setStatus(false);
                    }
                }else {
                    result.setMessage("Generate OTP and try again");
                    result.setStatusCode(HttpStatus.OK);
                    result.setStatus(false);
                }

            } else {
                result.setStatus(false);
                result.setMessage("Incorrect OTP");
                result.setStatusCode(HttpStatus.OK);
            }
        }
        return result;
    }

    @Override
    public Result sendOrderConfirmationSMS(OrderDetails orderDetails) {
        try {
            if (orderDetails != null) {
                boolean anyDeliveryToBeDone = false;
                int itemCount=0;
                if (orderDetails.getLines() != null) {
                    for (Line item : orderDetails.getLines()) {
                        if (item.getType() != null
                                && item.getType().trim().equalsIgnoreCase("delivery")) {
                            anyDeliveryToBeDone = true;
                            ++itemCount;
                        }
                    }
                }

                long totalGrossAmount = Math.round(orderDetails.getTotalGrossAmount());
                String billID = orderDetails.getBillID();
/* commented as per requirement to count items which are for "delivery"
                long items = orderDetails.getLines().size();
*/
                long items = itemCount;

                String userMobileNumber = orderDetails.getMobile();
                String mobileNumberInMessage = "9513755935";//"76767 98989";
                String contactDurationInMessage = "(9 AM to 10 PM)";

                Result result = new Result();
                //API KEY:
                String authKey = "175760AOr5cqO1fU0N59c39104";

                //Sender ID,While using route4 sender id should be 6 characters long.
                String senderId = "EVAMAL";

                //define route
                String route = "4";

                String message1 =
                        "Thanks for your purchase at " +
                                "Decathlon Brigade Road " +
                                "(Rs " + totalGrossAmount + "). " +
                                "Your bill ID is " +
                                billID + ". " +
                                "Get a copy of your bill at Welcome Desk if needed.";

                String itemText=items>1?items +" items":items+ " item";
                String message2="Your order ("+
                        itemText+") has been placed. "+
                        "To track your order "+
                        "("+billID+"), "+
                        "please call at "+
                        mobileNumberInMessage+" "+
                        contactDurationInMessage +". For all returns visit Decathlon Brigade Road.";

                boolean status1, status2 = false;
                status1 = sendMessage(message1, authKey, userMobileNumber, route, senderId);
                if (anyDeliveryToBeDone) {
                    status2 = sendMessage(message2, authKey, userMobileNumber, route, senderId);
                }

                if (status1 || status2) {
                    result.setMessage("Order Confirmation SMS Sent");
                    result.setStatus(true);

                } else {
                    result.setStatus(false);
                    result.setMessage("Error occurred while sending OTP");
                }
                return result;
            } else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void sendMessageAboutTesseractChildCategoryUpdate(String message) {
        try {
            if (message != null) {

                Result result = new Result();
                //API KEY:
                String authKey = "175760AOr5cqO1fU0N59c39104";

                //Sender ID,While using route4 sender id should be 6 characters long.
                String senderId = "EVAMAL";

                //define route
                String route = "4";

                sendMessage(message, authKey, "8197859846", route, senderId);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean sendMessage(String message, String authKey,
                                String userMobileNumber,
                                String route, String senderId) {
        try {
            //Prepare Url
            URLConnection myURLConnection = null;
            URL myURL = null;
            BufferedReader reader = null;

            //encoding message
            String encodedMessage = URLEncoder.encode(
                    message,
                    java.nio.charset.StandardCharsets.UTF_8.toString());

            //Send SMS API
            String mainUrl = "http://api.msg91.com/api/sendhttp.php?";

            //Prepare parameter string for message 1
            StringBuilder sbPostData = new StringBuilder(mainUrl);
            sbPostData.append("authkey=" + authKey);
            sbPostData.append("&mobiles=" + userMobileNumber);
            sbPostData.append("&message=" + encodedMessage);
            sbPostData.append("&route=" + route);
            sbPostData.append("&sender=" + senderId);

            //final string
            mainUrl = sbPostData.toString();
            try {
                //prepare connection
                myURL = new URL(mainUrl);
                myURLConnection = myURL.openConnection();
                myURLConnection.connect();
                reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
                //reading response
                String response;
                while ((response = reader.readLine()) != null) {
                    System.out.println(response);
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Product> searchProduct(String searchString) {
        try {
            List<Product> products =
                    productRepository.searchProductInCategoryAndProduct(searchString);
            if (products != null)
                return products;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Result getPlacemarkByModelCode(String modelCode) {
        Result result = new Result();
        try {
            List<Product> products = productRepository.findProductsByModelCode(modelCode);
            if (products != null && products.size() > 0) {
                Placemark placemark = products.get(0).getPlacemark();
                if (placemark != null) {
                    result.setStatus(true);
                    result.setResultObject(placemark);
                } else {
                    result.setStatus(false);
                    result.setMessage("No placemark found");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.setStatus(false);
            result.setMessage("Error occurred while fetching data");
        }
        return result;
    }

    @Override
    public boolean checkProductAvailability(Category category) {

        boolean available = false;
        try {
            CategoryDTO parentCategoryDTO =
                    CategoryAssembler.getCategoryDTOFromCategory(category);
            if (category.getSubCategories() != null && category.getSubCategories().size() > 0) {
                for (Category category1 : category.getSubCategories()) {
                    if (category1.getCategoryID() != null && Constants.CATEGORY_IDS_FOR_UPDATE.indexOf(category1.getCategoryID()) > -1) {
                        /*We don't need to use Root Categories as subcategory*/
                        continue;
                    }
                    CategoryDTO categoryDTO =
                            CategoryAssembler.getCategoryDTOFromCategory(category1);
                    if (categoryDTO.getStatus() == 1 && (category1.getSubCategories().size() > 0 || categoryDTO.getProducts().size() > 0)) {
                        available = checkProductAvailability(category1);
                    }
                }
            } else if (parentCategoryDTO.getProducts() != null && parentCategoryDTO.getProducts().size() > 0) {
                available = true;
            }
            return available;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateInventory(String modelCode, String placemarkID) {
        boolean updated = false;
        try {
            if (modelCode != null && placemarkID != null && !modelCode.isEmpty() && !placemarkID.isEmpty()) {
                Inventory inventoryData = inventoryRepository.findInventoryByModelCode(modelCode);

                if (inventoryData != null) {
                    /*data*/
                    inventoryData.setPlacemarkID(placemarkID);
                    Inventory inventory = inventoryRepository.saveAndFlush(inventoryData);
                    if (inventory != null)
                        updated = true;
                } else {
                    Inventory newInventoryData = new Inventory();
                    newInventoryData.setModelCode(modelCode);
                    try {
                        newInventoryData.setPlacemarkID(placemarkID);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Inventory inventory = inventoryRepository.saveAndFlush(newInventoryData);
                    if (inventory != null)
                        updated = true;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return updated;
    }

    @Override
    public OneIdTokenResponse getAccessToken() {

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(Constants.SPID_PRODUCTION_REFRESH_TOKEN_URL);
        Map<String, String> parameters = new HashMap<>();
        parameters.put(OneIdConstants.ACCESS_TOKEN_USERNAME_KEY, OneIdConstants.ACCESS_TOKEN_USERNAME_VALUE);
        parameters.put(OneIdConstants.ACCESS_TOKEN_GRANT_TYPE_KEY, OneIdConstants.ACCESS_TOKEN_GRANT_TYPE_VALUE);
        parameters.put(OneIdConstants.ACCESS_TOKEN_PASSWORD_KEY, OneIdConstants.ACCESS_TOKEN_PASSWORD_VALUE);
        parameters.put(OneIdConstants.ACCESS_TOKEN_SCOPE_KEY, OneIdConstants.ACCESS_TOKEN_SCOPE_VALUE);
        if (!parameters.isEmpty()) {
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                builder.queryParam(entry.getKey(), entry.getValue());
            }
        }
        HttpHeaders headers = new HttpHeaders();
        headers.set(OneIdConstants.AUTHORIZATION, OneIdConstants.AUTH_TOKEN);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        HttpEntity<OneIdTokenResponse> response = restClient.exchange(
                builder.build().encode().toUri(),
                HttpMethod.POST,
                entity,
                OneIdTokenResponse.class);
        return response.getBody();
    }

    @Override
    public Result getCustomerDetails(SearchCustomerDTO customer) {
        Result result = new Result();
        String mobile = customer.getMobile();
        /*OneIdTokenResponse tokenReponse = getAccessToken();*/
       String token;
       User user = userRepository.findUserByMobile(mobile);
        if (user!=null && user.getToken()!=null){
            token = user.getToken();
        }else {
            token = getOneIdAccess(user);
            user.setToken(token);
            userRepository.save(user);
        }

        String authToken = "EMPLOYEE " + token;
        HttpHeaders headers = new HttpHeaders();
        headers.add(OneIdConstants.API_KEY_HEADER, OneIdConstants.API_KEY);
        headers.add(HttpHeaders.AUTHORIZATION,  "Bearer " + token);
        headers.add(HttpHeaders.AUTHORIZATION, authToken);
        headers.setContentType(MediaType.APPLICATION_JSON);

        SearchCustomer customerDTO = new SearchCustomer();
        customerDTO.setMobile(mobile);

        HttpEntity<SearchCustomer> entity = new HttpEntity<>(customerDTO, headers);
        LOG.info("Request Body "+entity.getBody());
        LOG.info("Request Headers "+entity.getHeaders());
        try{
            JsonNode response = restClient.postForObject(OneIdConstants.SEARCH_CUSTOMER_URL, entity, JsonNode.class);
            if (null != response) {
                /*JsonNode node = getJson(response);*/
                result.setResultObject(response);
                result.setMessage(OneIdConstants.SUCCESS);
                result.setStatusCode(HttpStatus.OK);
            }
        }
         catch (HttpClientErrorException e){
            LOG.info(e.getResponseBodyAsString());
            JsonNode node = getJson(e.getResponseBodyAsString());
            if (node.has("error")){
                if (node.get("error").has("code")){
                    Long errorCode = node.get("error").get("code").asLong();
                    if (errorCode==4012){
                        getOneIdAccess(user);
                        result = getCustomerDetails(customer);
                    }else {
                        result.setResultObject(node);
                        result.setMessage(OneIdConstants.FAIL);
                        result.setStatusCode(e.getStatusCode());
                    }
                }
            }else {
                result.setResultObject(node);
                result.setMessage(OneIdConstants.FAIL);
                result.setStatusCode(e.getStatusCode());
            }
        }
        return result;
    }

    @Override
    public Result createCustomer(CreateCustomer customer) {
        Result result = new Result();
        String token;
        User user = userRepository.findUserByMobile(getUsername());
        if (user!=null&user.getToken()!=null){
            token = user.getToken();
        }else {
            token = getOneIdAccess(user);
            user.setToken(token);
            userRepository.save(user);
        }
        String authToken = "Bearer " + token;
        CreateCustomerDTO customerDTO = new CreateCustomerDTO();
        customerDTO.setAbo_fid(customer.isAbo_fid());
        customerDTO.setBilling_address(customer.getBilling_address());
        customerDTO.setContact(customer.getContact());
        customerDTO.setIdentity_individual(customer.getIdentity_individual());
        customerDTO.setOptin(customer.getOptin());
        customerDTO.setStores(customer.getStores());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(OneIdConstants.API_KEY_HEADER, OneIdConstants.API_KEY);
        headers.add(HttpHeaders.AUTHORIZATION, authToken);

        HttpEntity<CreateCustomerDTO> entity = new HttpEntity<>(customerDTO, headers);
        try {
            JsonNode response = restClient.postForObject(OneIdConstants.CREATE_CUSTOMER_URL, entity, JsonNode.class);
            if (null != response) {
                /*JsonNode node = getJson(response);*/
                result.setResultObject(response);
                result.setMessage(OneIdConstants.SUCCESS);
                result.setStatusCode(HttpStatus.CREATED);

            } else {
                result.setResultObject("");
                result.setMessage(OneIdConstants.FAIL);
                result.setStatusCode(HttpStatus.GONE);
            }
        } catch (HttpClientErrorException e) {
                JsonNode node = getJson(e.getResponseBodyAsString());
                if (node.has("error")){
                    if (node.get("error").has("code")){
                        Long errorCode = node.get("error").get("code").asLong();
                        if (errorCode==4012){
                            getOneIdAccess(user);
                            result = createCustomer(customer);
                        }
                    }
                }else {
                    result.setResultObject(getJson(e.getResponseBodyAsString()));
                    result.setMessage(OneIdConstants.FAIL);
                    result.setStatusCode(e.getStatusCode());
                }



        }
        return result;
    }

    @Override
    public Result updateCustomerIdentity(UpdateIdentityDTO dto) {
        Result result = new Result();
        restClient.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        String token;
        User user = userRepository.findUserByMobile(getUsername());
        if (user!=null&user.getToken()!=null){
            token = user.getToken();
        }else {
            token = getOneIdAccess(user);
            user.setToken(token);
            userRepository.save(user);
        }
        /*OneIdTokenResponse tokenReponse = getAccessToken();*/
        String authToken = "EMPLOYEE " + token;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(OneIdConstants.API_KEY_HEADER, OneIdConstants.API_KEY);
        headers.add(HttpHeaders.AUTHORIZATION, authToken);
        UpdateIdentityDTO identityDTO = new UpdateIdentityDTO();

        Map<String, Object> params = new HashMap<>();
        params.put(UpdateCustomerIdentity.BIRTHDATE.key(), dto.getBirthdate());
        params.put(UpdateCustomerIdentity.COUNTRY_CODE.key(), dto.getCountry_code());
        params.put(UpdateCustomerIdentity.LANGUAGE.key(), dto.getLanguage_code());
        params.put(UpdateCustomerIdentity.NAME.key(), dto.getName());
        params.put(UpdateCustomerIdentity.NAME2.key(), dto.getName2());
        params.put(UpdateCustomerIdentity.NATIONAL_ID_TYPE.key(), dto.getNational_id_type());
        params.put(UpdateCustomerIdentity.PREFIX.key(), dto.getPrefix());
        params.put(UpdateCustomerIdentity.SURNAME.key(), dto.getSurname());
        params.put(UpdateCustomerIdentity.MEMBER_TYPE.key(), dto.getMember_type());

        HttpEntity<Map> entity = new HttpEntity<>(params, headers);//identityDTO
        try {
            ResponseEntity<JsonNode> response = restClient.exchange(OneIdConstants.UPDATE_IDENTITY_URL + dto.getId_person(),
                    HttpMethod.PUT,
                    entity,
                    JsonNode.class,
                    params);
            if (null != response) {
                /*JsonNode node = getJson(response.getBody());*/
                result.setResultObject(response.getBody());
                result.setMessage(OneIdConstants.SUCCESS);
                result.setStatusCode(response.getStatusCode());
            }
        } catch (HttpClientErrorException e) {
            JsonNode node = getJson(e.getResponseBodyAsString());
                result.setResultObject(node);
                result.setMessage(OneIdConstants.FAIL);
                result.setStatusCode(e.getStatusCode());
            }

        return result;
    }

    @Override
    public Result updateCustomerContact(UpdateCustomerContact contact) {
        Result result = new Result();
        String token;
        User user = userRepository.findUserByMobile(getUsername());
        if (user!=null&user.getToken()!=null){
            token = user.getToken();
        }else {
            token = getOneIdAccess(user);
            user.setToken(token);
            userRepository.save(user);
        }
        String authToken = "EMPLOYEE " + token;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(OneIdConstants.API_KEY_HEADER, OneIdConstants.API_KEY);
        headers.add(HttpHeaders.AUTHORIZATION, authToken);

        UpdateCustomerContact customerContact = new UpdateCustomerContact();
        customerContact.setEmail(contact.getEmail());
        customerContact.setEmail_valid(contact.isEmail_valid());
        customerContact.setMobile(contact.getMobile());
        customerContact.setMobile_valid(contact.isMobile_valid());
        customerContact.setMobile_country_code(contact.getMobile_country_code());

        HttpEntity<UpdateCustomerContact> entity = new HttpEntity<>(customerContact, headers);
        try {
            ResponseEntity<JsonNode> response = restClient.exchange(OneIdConstants.UPDATE_CONTACT_URL + contact.getId_person(),
                    HttpMethod.PUT,
                    entity,
                    JsonNode.class);
            if (null != response) {
                /* JsonNode node = getJson(response.getBody());*/
                result.setResultObject(response.getBody());
                result.setMessage(OneIdConstants.SUCCESS);
                result.setStatusCode(response.getStatusCode());

            }
        } catch (HttpClientErrorException e) {
                result.setResultObject(getJson(e.getResponseBodyAsString()));
                result.setMessage(OneIdConstants.FAIL);
                result.setStatusCode(e.getStatusCode());
        }
        return result;
    }

    @Override
    public Result updateCustomerAddress(Map<String, Object> adderss) {

        Result result = new Result();
        Map<String, Object> params = new HashMap<>();

        int i = UpdateAddressKeys.values().length - 1;
        String[] keys = new String[UpdateAddressKeys.values().length];
        for (UpdateAddressKeys value : UpdateAddressKeys.values()) {
            keys[i--] = value.keys();
        }
        for (String key : keys) {
            if (adderss.containsKey(key)) {
                params.put(key, adderss.get(key));
            }
        }

        String token;
        User user = userRepository.findUserByMobile(getUsername());
        if (user!=null&user.getToken()!=null){
            token = user.getToken();
        }else {
            token = getOneIdAccess(user);
            user.setToken(token);
            userRepository.save(user);
        }
        String authToken = "EMPLOYEE " + token;

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(OneIdConstants.API_KEY_HEADER, OneIdConstants.API_KEY);
        headers.add(HttpHeaders.AUTHORIZATION, authToken);

        HttpEntity<?> entity = new HttpEntity<>(params, headers);
        try {
            ResponseEntity<JsonNode> response = restClient.exchange(
                    OneIdConstants.UPDATE_ADDRESS_URL + adderss.get("id_person"),
                    HttpMethod.PUT,
                    entity,
                    JsonNode.class);
            if (null != response) {
                /*JsonNode node = getJson(response.getBody());*/
                result.setResultObject(response.getBody());
                result.setMessage(OneIdConstants.SUCCESS);
                result.setStatusCode(response.getStatusCode());
            }
        } catch (HttpClientErrorException e) {
                result.setResultObject(getJson(e.getResponseBodyAsString()));
                result.setMessage(OneIdConstants.FAIL);
                result.setStatusCode(e.getStatusCode());
        }
        return result;
    }

    @Override
    public Boolean checkUser(SearchCustomerDTO dto) {
        if (!getUsername().equals(dto.getMobile())) {
            return false;
        } else
            return true;
    }

    @Override
    public Long checkUser() {
        SearchCustomerDTO customerDTO = new SearchCustomerDTO();
        customerDTO.setMobile(getUsername());
        Result result = getCustomerDetails(customerDTO);
        JsonNode node = (JsonNode) result.getResultObject();
        JsonNode customers = node.get("customers");
        if (customers != null && !(customers instanceof NullNode)) {
            ArrayNode nodes = (ArrayNode) node.get("customers");
            return nodes.get(0).get("id_person").asLong();
            /*return nodes.findValue("id_person").asLong();*/
        } else {
            return null;
        }
    }

    @Override
    public String getUsername() {
        String auth = request.getHeader("Authorization");
        Claims claims = JwtUtil.getClaims(auth);
        return claims.get("uid").toString();
    }

    private String getOneIdAccess(User user){
        OneIdTokenResponse tokenReponse = getAccessToken();
        user.setToken(tokenReponse.getAccess_token());
        userRepository.save(user);
        return tokenReponse.getAccess_token();
    }
}


