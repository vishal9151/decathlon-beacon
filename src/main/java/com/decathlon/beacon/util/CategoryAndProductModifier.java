package com.decathlon.beacon.util;

import com.decathlon.beacon.domain.category.Category;
import com.decathlon.beacon.domain.placemark.Placemark;
import com.decathlon.beacon.domain.product.Product;
import com.decathlon.beacon.domain.store.Inventory;
import com.decathlon.beacon.models.*;
import com.decathlon.beacon.repository.InventoryRepository;
import com.decathlon.beacon.repository.PlacemarkRepository;
import com.decathlon.beacon.schedule.Timely;
import com.decathlon.beacon.services.CommonService;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Component
public class CategoryAndProductModifier {
    private static final Logger log = LoggerFactory.getLogger(Timely.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    CommonService commonService;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlacemarkRepository placemarkRepository;

    private ArrayList<TesseractCategory> tesseractCategoryArrayList;
    private Map<String, TesseractCategory> tesseractCategoryMap=new HashMap<>();
    private ArrayList<TesseractNavigation> navigationTesseractsArrayList;
    private final String SECRET_KEY_FOR_IMAGE_URL_CREATION="pixl@cdn2securityKey";
    private long counterSuccessfulNewCategoryUpdate, counterSuccessfulOldCategoryUpdate,
            counterFailedCategoryUpdate,
            counterSuccessfulProductUpdate, counterFailedProductUpdate;

    private ArrayList<String> categoryIdGettingUpdatedMultipleTimes
            =new ArrayList<>();

    /**
     * @attribute  timeInMillisWhenReceivedSPIDAuthorization will help in taking decision when we need to refresh Authorization*/
    private long timeInMillisWhenReceivedSPIDAuthorization,
            timeInMillisWhenReceivedAuthoriationForReview;
    private String SPIDAuthoriztion, ReviewAuthorization;

    public void modifyCategoryAndProductDetails() throws Exception{
        /*this.restTemplate=restTemplate;
        this.commonService=commonService;*/
        /*Authorization*/
        SPIDAuthoriztion = getSPIDAuthorization();
        if (SPIDAuthoriztion != null && !SPIDAuthoriztion.isEmpty()) {
            timeInMillisWhenReceivedSPIDAuthorization=
                    Calendar.getInstance().getTimeInMillis();
            /*Tesseract child category api refresh*/
            String childCategoryAPIResponse = getTesseractChildCategory(SPIDAuthoriztion);
            if (childCategoryAPIResponse != null && !childCategoryAPIResponse.isEmpty()) {

                TesseractCategory[] tesseractCategories =
                        new Gson().fromJson(childCategoryAPIResponse, TesseractCategory[].class);
                if (tesseractCategories != null && tesseractCategories.length > 0) {
                    String tesseractNavigationAPIResponse=tryToGetTesseractNavigation(SPIDAuthoriztion);
                    if(tesseractNavigationAPIResponse!=null &&
                            !tesseractNavigationAPIResponse.isEmpty()){
                        TesseractNavigation [] navigationTesseracts=
                                new Gson().fromJson(tesseractNavigationAPIResponse,TesseractNavigation[].class);
                        if(navigationTesseracts!=null && navigationTesseracts.length>0){
                            navigationTesseractsArrayList=
                                    new ArrayList<TesseractNavigation>(Arrays.asList(navigationTesseracts));
                        }
                    }
                    tesseractCategoryArrayList =
                            new ArrayList<TesseractCategory>(Arrays.asList(tesseractCategories));

                    tesseractCategoryMap=new HashMap<>();
                    for(TesseractCategory subTesseractCategory:tesseractCategoryArrayList){
                        if(subTesseractCategory.getId()!=null){
                            tesseractCategoryMap.put(subTesseractCategory.getId().trim(), subTesseractCategory);
                        }
                    }

                        /*assumption is we have received valid set of categories*/
                    updateStatusOfAllSportsCategoryToDisabled();
                    updateCategoryAndProductDetails(tesseractCategoryArrayList);
                }
            } else {
                log.error(Constants.ERROR_WHILE_RUNNING_CRON_FOR_CATEGORY_UPDATE);
            }
        } else {
            log.error(Constants.ERROR_WHILE_RUNNING_CRON_FOR_CATEGORY_UPDATE);
        }
    }

    private void updateCategoryAndProductDetails(
            ArrayList<TesseractCategory> tesseractCategoryArrayList) {
        try{
            /*find root category i.e. All Sports*/
            TesseractCategory tesseractCategoryToUpdate=null;
            for (TesseractCategory tesseractCategory:tesseractCategoryArrayList){
                if(tesseractCategory.getId()!=null &&
                        Constants.CATEGORY_IDS_FOR_UPDATE.indexOf(tesseractCategory.getId())>-1){
                    tesseractCategoryToUpdate=tesseractCategory;
                    if(tesseractCategoryToUpdate!=null){
                        commonService.sendMessageAboutTesseractChildCategoryUpdate(
                                Constants.MESSAGE_START_OF_TESSERACT_CHILD_CATEGORY_UPDATE +" at : "+
                                        Calendar.getInstance().getTimeInMillis());
                        updateDBWithTesseractCategoryAndProducts(tesseractCategoryToUpdate,true);
                        commonService.sendMessageAboutTesseractChildCategoryUpdate(
                                Constants.MESSAGE_SUCCESSFUL_ATTEMPT_TO_UPDATE_TESSERACT_CHILD_CATEGORY +" at: "+
                                        Calendar.getInstance().getTimeInMillis());
                    }
                }
            }

        }catch (Exception e){
            commonService.sendMessageAboutTesseractChildCategoryUpdate(
                    Constants.MESSAGE_PROBLEM_WHILE_UPDATE_TESSERACT_CHILD_CATEGORY +" at: "+
                            Calendar.getInstance().getTimeInMillis());
            e.printStackTrace();
        }
    }

    private void updateDBWithTesseractCategoryAndProducts(TesseractCategory tesseractCategory,boolean rootCategory) {
        try{

            if(tesseractCategory!=null){
                String categoryID=tesseractCategory.getId();
                Long beaconCatogoryID=null;

                Category categoryAvailableInDB=
                        commonService.getCategoryByCategoryID(
                                categoryID);
                String thumbnailID=null,imageURL=null;
                if(navigationTesseractsArrayList!=null &&
                        navigationTesseractsArrayList.size()>0){
                    for(TesseractNavigation tesseractNavigation:
                            navigationTesseractsArrayList){
                        TesseractNavigation.MetaData metaData=
                                tesseractNavigation.getMetaData();
                        if(metaData!=null && metaData.getCategoryId()!=null
                                && metaData.getCategoryId().trim().equals(categoryID)){
                            thumbnailID=metaData.getThumbnailId();
                            break;
                        }
                    }
                }

                if(thumbnailID!=null && !thumbnailID.isEmpty()){
                    String sThumbnailID=/*"s"+*/thumbnailID;/*thumbnailID is already prefixed with s, so its not required to prefix again with s*/
                    String key=null;
                    try {
                        key=hmacDigest(sThumbnailID,SECRET_KEY_FOR_IMAGE_URL_CREATION,"HmacMD5");
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                    if(key!=null){
                        imageURL=
                                Constants.CONSTANT_PART_OF_IMAGE_URL
                                        +sThumbnailID+"/k$"+key+"/"+300+"x"+300;
                    }
                }

                if(categoryAvailableInDB!=null){
                    categoryAvailableInDB.setStatus(1);
                    categoryAvailableInDB.setUpdatedOn(Helper.getCurrentTimeStamp());
                    if(tesseractCategory.getTranslations()!=null && tesseractCategory.getTranslations().size()>0){
                        String nameFromTranslation = tesseractCategory.getTranslations().get(0).getLabel();
                        categoryAvailableInDB.setName(nameFromTranslation);
                    } else {
                        categoryAvailableInDB.setName(tesseractCategory.getName());
                    }
                    if(thumbnailID!=null && !thumbnailID.isEmpty()) {
                        categoryAvailableInDB.setThumbnailID(thumbnailID);
                        if(imageURL!=null && !imageURL.isEmpty())
                            categoryAvailableInDB.setImageUrl(imageURL);
                    }
                    Category category=commonService.saveCategory(categoryAvailableInDB);
                    if(category!=null) {
                        beaconCatogoryID = category.getId();
                        categoryIdGettingUpdatedMultipleTimes.add(categoryID);
                    }
                    updateStatus(category,categoryAvailableInDB,false);

                }else {
                    categoryAvailableInDB=new Category();
                    categoryAvailableInDB.setCategoryID(categoryID);
                    String hierachicalParentID=tesseractCategory.getHierarchicalParentId();
                    if(hierachicalParentID!=null)
                        categoryAvailableInDB.setHierarchicalParentID(hierachicalParentID);
                    categoryAvailableInDB.setStatus(1);
                    categoryAvailableInDB.setUpdatedOn(Helper.getCurrentTimeStamp());
                    if(tesseractCategory.getTranslations()!=null && tesseractCategory.getTranslations().size()>0){
                        String nameFromTranslation = tesseractCategory.getTranslations().get(0).getLabel();
                        categoryAvailableInDB.setName(nameFromTranslation);
                    } else {
                        categoryAvailableInDB.setName(tesseractCategory.getName());
                    }
//                    categoryAvailableInDB.setName(tesseractCategory.getName());
                    if(thumbnailID!=null && !thumbnailID.isEmpty()) {
                        categoryAvailableInDB.setThumbnailID(thumbnailID);
                        if(imageURL!=null && !imageURL.isEmpty())
                            categoryAvailableInDB.setImageUrl(imageURL);
                    }
                    if(!rootCategory){
                        Category parentCategory=commonService.getCategoryByCategoryID(
                                hierachicalParentID);
                        if(parentCategory!=null){
                            long parentID=parentCategory.getId();
                            categoryAvailableInDB.setParentID(parentID);
                        }else{
                            /*Note: if parent is not available we cannot move further*/
                            return;
                        }
                    }else {
                        categoryAvailableInDB.setParentID((long) 1);
                    }
                    Category category=commonService.saveCategory(categoryAvailableInDB);
                    if(category!=null)
                        beaconCatogoryID=category.getId();
                    updateStatus(category,categoryAvailableInDB,true);
                }

                ArrayList<String> childCategoryIDs=tesseractCategory.getChildCategoryIds();
                ArrayList<String> itemIDs=tesseractCategory.getItemIds();


                if(childCategoryIDs!=null &&
                        childCategoryIDs.size()>0){
                    /*update DB with child categories*/
                    for(String subCategoryID:childCategoryIDs){
                        /*find TesseractCategory for subCategoryID*/
                        if(tesseractCategoryMap.containsKey(subCategoryID.trim())){
                            TesseractCategory childCategory = tesseractCategoryMap.get(subCategoryID);
                            updateDBWithTesseractCategoryAndProducts(childCategory,false);
                        }

//                        for(TesseractCategory subTesseractCategory:tesseractCategoryArrayList){
//                            if(subTesseractCategory.getId()!=null &&
//                                    subTesseractCategory.getId().trim().equalsIgnoreCase(subCategoryID.trim())){
//                                updateDBWithTesseractCategoryAndProducts(subTesseractCategory,false);
//                            }
//                        }
                    }

                }else if(itemIDs!=null &&
                        itemIDs.size()>0){
                    /*update DB with products under this category*/
                    for(String itemID:itemIDs){
                        try {
                            Calendar calendar = Calendar.getInstance();
                            long currentTimeInMillis = calendar.getTimeInMillis();
                            long difference = currentTimeInMillis - timeInMillisWhenReceivedSPIDAuthorization;
                            if (difference > 5400000) {
                            /*after 1.5 hours we will first refresh SPID authoriztion*/
                                String Authorization = getSPIDAuthorization();
                                if (Authorization != null && !Authorization.isEmpty()) {
                                    SPIDAuthoriztion = Authorization;
                                    timeInMillisWhenReceivedSPIDAuthorization =
                                            calendar.getTimeInMillis();
                                }
                            }

                            String productDetailsSPIDResponse =
                                    getProductDetailsSPID(SPIDAuthoriztion, itemID,false);
                            if(productDetailsSPIDResponse!=null &&
                                    !productDetailsSPIDResponse.isEmpty())
                            {
                                ProductInfoSPID productInfoSPID=new Gson().fromJson(productDetailsSPIDResponse,
                                        ProductInfoSPID.class);
                                if(productInfoSPID!=null) {
                                    String productModelCode = itemID;
                                    String name=null,brandName=null,pixlID=null
                                            ,productImageUrl=null;
                                    Long brandID=null,placemarkID=null;
                                    if(productInfoSPID.getWebLabel()!=null)
                                        name=productInfoSPID.getWebLabel();
                                    ProductInfoSPID.Brand brand=productInfoSPID.getBrand();
                                    if(brand!=null) {
                                        brandID = productInfoSPID.getBrand().getId();
                                        brandName = productInfoSPID.getBrand().getName();
                                    }
                                    ProductInfoSPID.FirstPackShotWeb firstPackShotWeb=
                                            productInfoSPID.getFirstPackShotWeb();
                                    if(firstPackShotWeb!=null){
                                        ProductInfoSPID.FirstPackShotWeb.Packshot packshot=
                                                firstPackShotWeb.getPackshot();
                                        if(packshot!=null){
                                            pixlID=packshot.getPixlId();
                                            if(pixlID!=null && !pixlID.isEmpty()){
                                                String pPixlID="p"+pixlID;
                                                String key=null;
                                                try {
                                                    key=hmacDigest(pPixlID,SECRET_KEY_FOR_IMAGE_URL_CREATION,
                                                            "HmacMD5");
                                                } catch (Throwable throwable) {
                                                    throwable.printStackTrace();
                                                }
                                                if(key!=null){
                                                    productImageUrl=
                                                            Constants.CONSTANT_PART_OF_IMAGE_URL
                                                                    +pPixlID+"/k$"+key+"/"+300+"x"+300;
                                                }
                                            }
                                        }
                                    }

                                    Double price=null;
                                    boolean inRange=false;
                                    String productItemID=null,ean=null,size=null;

                                    String productOtherDetails=getProductOtherDetails(productModelCode);
                                    if(productOtherDetails!=null && !productOtherDetails.isEmpty()){
                                        try {

                                            ProductOtherDetails otherDetails =
                                                    new Gson().fromJson(productOtherDetails,
                                                            ProductOtherDetails.class);
                                            if (otherDetails!=null){
                                                if(otherDetails.getItems()!=null &&
                                                        otherDetails.getItems().size()>0){
                                                    ProductOtherDetails.Item item=
                                                            otherDetails.getItems().get(0);
                                                    try {
                                                        String individualItemPrice = item.getSalePrice();
                                                        if (individualItemPrice != null &&
                                                                !individualItemPrice.trim().isEmpty()) {
                                                            price = Double.valueOf(item.getSalePrice());
                                                        }
                                                    }catch (Exception e){
                                                        e.printStackTrace();
                                                    }
                                                    productItemID=item.getId();
                                                    ean=item.getEan();
                                                    inRange=item.isInRange();
                                                    size=item.getSize();
                                                }
                                            }
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }
                                    }
                                    currentTimeInMillis = calendar.getTimeInMillis();
                                    long differenceForReviewAuthorization =
                                            currentTimeInMillis - timeInMillisWhenReceivedAuthoriationForReview;
                                    if (differenceForReviewAuthorization > 5400000) {
                            /*after 1.5 hours we will refresh review get authorization*/
                                        String Authorization=
                                                getReviewAPIAuthorization();
                                        if(Authorization!=null &&
                                                !Authorization.isEmpty()){
                                            ReviewAuthorization=Authorization;
                                            timeInMillisWhenReceivedAuthoriationForReview=calendar.getTimeInMillis();
                                        }
                                    }

                                    String reveiwApiResponse=null;
                                    if(ReviewAuthorization!=null &&
                                            !ReviewAuthorization.isEmpty()){
                                        reveiwApiResponse=
                                                getReviewForProduct(
                                                        ReviewAuthorization,
                                                        productModelCode);
                                    }

                                    Inventory inventoryData=
                                            inventoryRepository.findInventoryByModelCode(productModelCode);
                                    if(inventoryData!=null){
                                        String inventoryDataPlacemarkID=inventoryData.getPlacemarkID();
                                        if(inventoryDataPlacemarkID!=null && !inventoryDataPlacemarkID.isEmpty()){
                                            Placemark placemark=placemarkRepository.
                                                    findPlacemarkByPlacemarkId(inventoryDataPlacemarkID);
                                            if(placemark!=null){
                                                placemarkID=placemark.getId();
                                            }
                                        }

                                    }


                                    Product productAvailableInDB =
                                            commonService.getProductByModelCode(productModelCode);
                                    boolean productFoundInDB=false;
                                    if (productAvailableInDB != null) {
                                        productFoundInDB=true;
                                    }else {
                                        productAvailableInDB=new Product();
                                    }

                                    /*update product object*/
                                    if(name!=null)
                                        productAvailableInDB.setName(name);
                                    productAvailableInDB.setModelCode(productModelCode);
                                    if(pixlID!=null && !pixlID.isEmpty())
                                        productAvailableInDB.setPixlID(pixlID);
                                    if(productImageUrl!=null && !productImageUrl.isEmpty())
                                        productAvailableInDB.setImageUrl(productImageUrl);
                                    if(size!=null && !size.isEmpty())
                                        productAvailableInDB.setSize(size);
                                    if(ean!=null && !ean.isEmpty())
                                        productAvailableInDB.setEan(ean);
                                    productAvailableInDB.setInRange(inRange);
                                    if(price!=null)
                                        productAvailableInDB.setPrice(price);
                                    if(brandID!=null)
                                        productAvailableInDB.setBrandId(brandID);
                                    if(brandName!=null)
                                        productAvailableInDB.setBrandName(brandName);
                                    if(reveiwApiResponse!=null && !reveiwApiResponse.isEmpty())
                                        productAvailableInDB.setReview(reveiwApiResponse);
                                    if(productItemID!=null && !productItemID.isEmpty())
                                        productAvailableInDB.setItemCode(productItemID);
                                    productAvailableInDB.setStatus(1);

                                    /*update product with parent category*/
                                    productAvailableInDB.setCategoryID(beaconCatogoryID);

                                    if(placemarkID != null)
                                        productAvailableInDB.setArubaPlacemarkId(placemarkID);

                                    Product product=
                                            commonService.saveProduct(productAvailableInDB);

                                    //updateStatus(product,productAvailableInDB);

                                }
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }



        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void updateStatus(Category category,Category categoryAvailableInDB,boolean isAdditionOfNewCategory) {
        try{
            if(category!=null) {
                if(isAdditionOfNewCategory)
                    ++counterSuccessfulNewCategoryUpdate;
                else {
                    ++counterSuccessfulOldCategoryUpdate;
                    StringBuilder stringBuilder=new StringBuilder("Redundant category updates ");
                    for(String categoryID:categoryIdGettingUpdatedMultipleTimes){
                        stringBuilder.append(", "+categoryID+ ": "+category.getCategoryID());
                    }
                    log.error("Duplicate category updations for following: "+stringBuilder.toString());
                }
                log.info("Category : "+category.getName() + ", ID: "+ category.getCategoryID()+
                        ", "+counterSuccessfulNewCategoryUpdate+" new and "+
                        counterSuccessfulOldCategoryUpdate+" old category got updated.");
            }else {
                ++counterFailedCategoryUpdate;
                log.error("Category : "+categoryAvailableInDB.getName() + ", ID: "+
                        categoryAvailableInDB.getCategoryID()+", "+counterFailedCategoryUpdate+
                        " category failed to update");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateStatus(Product product,Product productAvailableInDB) {
        try{
            if(product!=null) {
                ++counterSuccessfulProductUpdate;
                log.info(product.getName() + " product model code : " + product.getModelCode()+ " got updated in DB. "+
                        counterSuccessfulProductUpdate+" product updated");
            }else {
                ++counterFailedProductUpdate;
                log.error(productAvailableInDB.getName() + " not updated in DB " +
                        counterFailedProductUpdate+
                        " product failed to update");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateStatusOfAllSportsCategoryToDisabled() {
        try{
                        /*
                        root category

                        "name": "All sports",
                        "id": "3ff3c64a-b62d-4d35-b891-9baf487c0023",
                        * */
            for(String categoryId : Constants.CATEGORY_IDS_FOR_UPDATE){
                Category category=
                        commonService.getCategoryByName(categoryId);
                if(category!=null){
                    updateCategoryStatusToDisabled(category,null,false);
                    updateDBWithUpdatedCategories(category);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateDBWithUpdatedCategories(Category allSprotsCategory) {
        try{
            updateCategoryStatusToDisabled(allSprotsCategory,null,true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateCategoryStatusToDisabled(Category category, Product product, boolean updateDBWithCategory) {
        try{
            if(category!=null) {
                if(!updateDBWithCategory) {
                    category.setStatus(0);
                }else {
                    Category savedCategory = commonService.saveCategory(category);
                }
                if (category.getSubCategories() != null &&
                        category.getSubCategories().size() > 0) {
                    for (Category subCategory : category.getSubCategories()) {
                        if(subCategory.getCategoryID()!=null
                                && Constants.CATEGORY_IDS_FOR_UPDATE.indexOf(subCategory.getCategoryID())>-1)
                            continue;
                        updateCategoryStatusToDisabled(subCategory,null,updateDBWithCategory);
                    }
                } else if (category.getProducts() != null &&
                        category.getProducts().size() > 0) {
                    for(Product productToUpdate:category.getProducts()){
                        updateCategoryStatusToDisabled(null,productToUpdate,updateDBWithCategory);
                    }
                }
            }else if(product!=null){
                if(!updateDBWithCategory) {
                    product.setStatus(0);
                }else {
                    Product savedProduct=commonService.saveProduct(product);
                    if(savedProduct!=null) {
                        /*to test*/
                        String name = savedProduct.getName();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public String getSPIDAuthorization(){
        try {
            HttpHeaders headers = new HttpHeaders();
        /*headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));*/
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", Constants.SPID_PRODUCTION_REFRESH_TOKEN_AUTHORIZATION);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("grant_type", "password");
            map.add("username", "india_apps");
            map.add("password", "Sw_tracH7Cuc");
            map.add("scope", "openid profile");

            HttpEntity<MultiValueMap<String,String>> entity =
                    new HttpEntity
                            <MultiValueMap<String, String>>(
                            map,headers);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(Constants.SPID_PRODUCTION_REFRESH_TOKEN_URL, HttpMethod.POST,entity,
                            String.class);
            if(responseEntity!=null){
                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    SPIDProductionRefreshAuthtokenResponse spidProductionRefreshAuthtokenResponse=
                            new Gson().fromJson(body,
                                    SPIDProductionRefreshAuthtokenResponse.class);
                    if(spidProductionRefreshAuthtokenResponse.getTokenType()!=null &&
                            spidProductionRefreshAuthtokenResponse.getAccessToken() !=null){
                        String authorization=spidProductionRefreshAuthtokenResponse.getTokenType()+" "+
                                spidProductionRefreshAuthtokenResponse.getAccessToken();
                        return authorization;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getReviewAPIAuthorization(){
        try {
            HttpHeaders headers = new HttpHeaders();
        /*headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));*/
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("_username", "Z01ARIYA");
            map.add("_password", "CCNASS");

            HttpEntity<MultiValueMap<String,String>> entity =
                    new HttpEntity<MultiValueMap<String, String>>(
                            map,headers);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(Constants.REVIEW_API_AUTH_TOKEN_URL, HttpMethod.POST,entity,
                            String.class);
            if(responseEntity!=null){
                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    ReviewAPIAuthorationResponse reviewAPIAuthorationResponse=
                            new Gson().fromJson(body,
                                    ReviewAPIAuthorationResponse.class);
                    if(reviewAPIAuthorationResponse!=null &&
                            reviewAPIAuthorationResponse.getToken()!=null &&
                            !reviewAPIAuthorationResponse.getToken().isEmpty()){
                        String authorization=reviewAPIAuthorationResponse.getToken();
                        return authorization;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }



    public String getTesseractChildCategory(String authorization){
        try{
            HttpHeaders httpHeaders=getHttpHeadersForDecathlonTesseract(authorization);

            HttpEntity entity =
                    new HttpEntity<>(httpHeaders);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(Constants.TESSERACT_CHILD_CATEGORY_PRODUCTION_URL,
                            HttpMethod.GET,entity,
                            String.class);

            if(responseEntity!=null){
                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    return body;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public String getProductDetailsSPID(String authorization,String modelCode,boolean forGlobalAPI){
        try{
            HttpHeaders httpHeaders=getHttpHeadersForProductDetailsSPID(authorization);

            HttpEntity entity =
                    new HttpEntity<>(httpHeaders);

            String url="";
            if(!forGlobalAPI)
                url=Constants.PRODUCT_DETAILS_SPID_PROD_URL;
            else
                url=Constants.PRODUCT_DETAILS_SPID_PROD_FOR_GLOBAL_URL;
            url=url.replace("{*******}",modelCode);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(url,
                            HttpMethod.GET,entity,
                            String.class);

            if(responseEntity!=null){


                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    if(forGlobalAPI){
                        log.info("on success of SPID Product get for Global modelCode: "+modelCode);
                    }
                    return body;
                }
            }else{

                handleUnexpectedCaseOfSPIDProductGet(forGlobalAPI,modelCode,authorization);

            }
        }catch (Exception e){
            log.error(Constants.ERROR_WHILE_PRODUCT_DETAILS_SPID_API);
            //e.printStackTrace();
            handleUnexpectedCaseOfSPIDProductGet(forGlobalAPI,modelCode,authorization);
        }
        return null;
    }

    private void handleUnexpectedCaseOfSPIDProductGet(
            boolean forGlobalAPI, String modelCode, String authorization) {
        if(!forGlobalAPI){
            log.info("on fail of spid prod in modelCode : "+modelCode);
                    /*if this api fails to get product details for IN geo, we will check at Global (GB) geo*/
            getProductDetailsSPID(authorization,modelCode,true);
        }else{
            if(forGlobalAPI){
                log.info("on fail of spid prod gn modelCode :"+modelCode);
            }
        }
    }

    private String getReviewForProduct(String authorization,
                                       String productModelCode) {
        try{

            authorization= "ovbearer "+authorization;

            HttpHeaders headers = new HttpHeaders();
        /*headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));*/
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", authorization);
            HttpEntity entity =
                    new HttpEntity<>(headers);

            String url=Constants.REVIEW_API_URL;
            url=url.replace("{*******}",productModelCode);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(url,
                            HttpMethod.GET,entity,
                            String.class);

            if(responseEntity!=null){
                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    return body;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private HttpHeaders getHttpHeadersForProductDetailsSPID(String authorization) {
        try{
            HttpHeaders headers = new HttpHeaders();
        /*headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));*/
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", authorization);
            headers.set("X-API-Key",Constants.PRODUCT_DETAILS_SPID_PROD_GET_API_KEY);
            headers.set("Cache-Control",Constants.NO_CACHE);
            return headers;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private HttpHeaders getHttpHeadersForDecathlonTesseract(String authorization) {
        try{
            HttpHeaders headers = new HttpHeaders();
        /*headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));*/
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", authorization);
            headers.set("X-Api-Key",Constants.TESSERACT_API_KEY);
            //headers.set("Geographic-Entity",Constants.GLOBAL_GEO_ENTITY);
            headers.set("Geographic-Entity",Constants.IN_GEO_ENTITY);
            return headers;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private String tryToGetTesseractNavigation(String authorization) {
        try{
            HttpHeaders httpHeaders=getHttpHeadersForDecathlonTesseract(authorization);

            HttpEntity entity =
                    new HttpEntity<>(httpHeaders);

            ResponseEntity<String> responseEntity =
                    restTemplate.exchange(Constants.TESSERACT_NAVIGATION_PRODUCTION_URL,
                            HttpMethod.GET,entity,
                            String.class);
            if(responseEntity!=null){
                String body= responseEntity.getBody();
                if(body!=null && !body.isEmpty()){
                    return body;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private String getProductOtherDetails(String productModelCode) {
        try{
            String url=Constants.PRODUCT_OTHER_DETAILS_LIKE_PRICE_ITEM_ID_URL;
            url=url.replace("{*******}",productModelCode);

            String otherProductDetails=restTemplate.getForObject(url,String.class);
            if(otherProductDetails!=null && !otherProductDetails.isEmpty()) {
                return otherProductDetails;
            }
        }catch (Exception e){
            log.error(Constants.ERROR_WHILE_GETTING_PRODUCT_OTHER_DETAILS);
            //e.printStackTrace();
        }
        return null;
    }


    public static String hmacDigest(String msg, String keyString, String algo) throws Throwable {
        String digest = null;

        if (keyString == null) {
            throw new Throwable();
        }
        if (keyString.equals("")) {
            throw new Throwable();
        }

        javax.crypto.spec.SecretKeySpec key = new javax.crypto.spec.SecretKeySpec((keyString).getBytes("UTF-8"), algo);
        javax.crypto.Mac mac = javax.crypto.Mac.getInstance(algo);
        mac.init(key);

        byte[] bytes = mac.doFinal(msg.getBytes("UTF-8"));

        java.util.Formatter formatter = new java.util.Formatter();
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        digest = formatter.toString();

        return digest;
    }


}
