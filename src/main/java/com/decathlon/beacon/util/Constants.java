package com.decathlon.beacon.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by VISHAL on 27-12-2017.
 */
public class Constants {

    public static final String IMAGE_BASE_URL = "https://contents.mediadecathlon.com/p";
    public static final String IMAGE_SECRETE_KEY = "/pixl@cdn2securityKey";
    public static final String IMAGE_SIZE = "/700x500";
    public static final String SPID_PRODUCTION_REFRESH_TOKEN_AUTHORIZATION=
            "Basic ZXZhbWFsbDpodXJCd1RSeXczMEF0QlhVVUVEQnZzRTV6MEZOc1ZHN1F5R2NXemlBbW9wUlRDODR5OTZJdGZVRHpld3JTUGFk";
    public static final String SPID_PRODUCTION_REFRESH_TOKEN_URL=
            "https://idpdecathlon.oxylane.com/as/token.oauth2";
    public static final String ERROR_WHILE_RUNNING_CRON_FOR_CATEGORY_UPDATE=
            "ERROR_WHILE_RUNNING_CRON_FOR_CATEGORY_UPDATE";
    public static final String TESSERACT_API_KEY = "102020f7-f18f-4a0b-93d3-a598ba07c20a";
    public static final String PRODUCT_DETAILS_SPID_PROD_GET_API_KEY = "dbaa232b-0eae-4629-abdd-43c817be298d";
    public static final String GLOBAL_GEO_ENTITY = "GLOBAL";
    public static final String IN_GEO_ENTITY = "IN";
    public static final String TESSERACT_CHILD_CATEGORY_PRODUCTION_URL =
            "https://api.decathlon.net/catalogs/categories/export?item_level=model";
    public static final String TESSERACT_NAVIGATION_PRODUCTION_URL =
            "https://api.decathlon.net/catalogs/navigations";
    public static final String PRODUCT_DETAILS_SPID_PROD_URL=
            "https://api.decathlon.net/spid/v2/models?modelCode={*******}&locale=en_IN&state=CURRENT";
    public static final String PRODUCT_DETAILS_SPID_PROD_FOR_GLOBAL_URL=
            "https://api.decathlon.net/spid/v2/models?modelCode={*******}&locale=en_GB&state=CURRENT";
    public static List<String> CATEGORY_IDS_FOR_UPDATE=new ArrayList<>();
//            "3ff3c64a-b62d-4d35-b891-9baf487c0023";
    static {
        CATEGORY_IDS_FOR_UPDATE.add("8c2709ee-a913-4502-8c80-1c8bbd66209b");
        CATEGORY_IDS_FOR_UPDATE.add("3362507d-2798-4873-a972-9b877674d66c");
        CATEGORY_IDS_FOR_UPDATE.add("512e879c-f499-47ad-93a7-09fb0ec5f15f");
        CATEGORY_IDS_FOR_UPDATE.add("c88b2d89-3ba4-47dc-bbc5-f5b4d208f606");
        CATEGORY_IDS_FOR_UPDATE.add("3ff3c64a-b62d-4d35-b891-9baf487c0023");
}

    public static final String IMAGE_URL_PATTERN=
            "https://contents.mediadecathlon.com/p{pixlID}{OR}s{thumbnailID}/k${key}/{height}x{width}";
    public static final String CONSTANT_PART_OF_IMAGE_URL="https://contents.mediadecathlon.com/";
    public static final String NO_CACHE = "no-cache";
    public static final String PRODUCT_OTHER_DETAILS_LIKE_PRICE_ITEM_ID_URL=
            "https://retailerp.decathlon.in/retail/ws/com.promantia." +
                    "decathlon.digital.getproduct?l=appeva1447&p=appeva1447&store_id=1447&model={*******}";
    public static final String REVIEW_API_AUTH_TOKEN_URL=
            "https://reviews.decathlon.com/api/post/en_IN/login_check";
    public static final String REVIEW_API_URL=
            "https://reviews.decathlon.com/api/en_IN/v2/review/list?" +
                    "type=product&source=dktin&offer={*******}";
    public static final String ERROR_WHILE_PRODUCT_DETAILS_SPID_API =
            "ERROR_WHILE_PRODUCT_DETAILS_SPID_API_CALL";
    public static final String ERROR_WHILE_GETTING_PRODUCT_OTHER_DETAILS=
            "ERROR_WHILE_GETTING_PRODUCT_OTHER_DETAILS";

    public static String MESSAGE_START_OF_TESSERACT_CHILD_CATEGORY_UPDATE=
            "MESSAGE_START_OF_TESSERACT_CHILD_CATEGORY_UPDATE";
    public static String MESSAGE_SUCCESSFUL_ATTEMPT_TO_UPDATE_TESSERACT_CHILD_CATEGORY=
            "MESSAGE_SUCCESSFUL_ATTEMPT_TO_UPDATE_TESSERACT_CHILD_CATEGORY";
    public static String MESSAGE_PROBLEM_WHILE_UPDATE_TESSERACT_CHILD_CATEGORY=
            "MESSAGE_PROBLEM_WHILE_UPDATE_TESSERACT_CHILD_CATEGORY";
}
