#-----------------------#
# Create Table Category
#-----------------------#

CREATE TABLE IF NOT EXISTS category(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name              VARCHAR(100)       NOT NULL,
  updated_on        TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,

  PRIMARY KEY (id),
  INDEX (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
;


#--------------------------#
# Create Table SubCategory
#--------------------------#

CREATE TABLE IF NOT EXISTS sub_category(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  category_id       MEDIUMINT UNSIGNED NOT NULL,
  name              VARCHAR(100)       NOT NULL,
  updated_on        TIMESTAMP          NOT NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,
  PRIMARY KEY (id),
  INDEX (id),
  KEY `FK_SUBCATEGORY_CATEGORY_ID` (category_id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
;

#-------------------#
#Create Table Brand
#-------------------#

CREATE TABLE IF NOT EXISTS brand(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  name              VARCHAR(100)       NOT NULL,
  updated_on        TIMESTAMP          NOT NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,
  PRIMARY KEY (id),
  INDEX (id)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;
;

#-------------------#
#Create Table Product
#-------------------#

CREATE TABLE IF NOT EXISTS product(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  brand_id          MEDIUMINT UNSIGNED NOT NULL,
  model_code        VARCHAR(60)        NOT NULL,
  item_code         VARCHAR(60)        NOT NULL,
  sub_category_id   MEDIUMINT UNSIGNED NOT NULL,
  name              VARCHAR(100)       NOT NULL,
  image_url         VARCHAR(255),
  price             DECIMAL(10,2),     NOT NULL DEFAULT 0,
  updated_on        TIMESTAMP          NOT NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,
  PRIMARY KEY (id),
  INDEX (id),
  KEY `FK_PRODUCT_BRAND_ID` (brand_id),
  KEY `FK_PRODUCT_SUB_CATEGORY_ID` (sub_category_id)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8;
;



#---------------------------#
# Alter Table SubCategory#
#---------------------------#
ALTER TABLE `sub_category`
  ADD CONSTRAINT `FK_SUBCATEGORY_CATEGORY_ID` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
  ON UPDATE CASCADE;

#---------------------------#
# Alter Table Product#
#---------------------------#
ALTER TABLE `product`
  ADD CONSTRAINT `FK_PRODUCT_BRAND_ID` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`id`)
  ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PRODUCT_SUB_CATEGORY_ID` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_category` (`id`)
  ON UPDATE CASCADE;