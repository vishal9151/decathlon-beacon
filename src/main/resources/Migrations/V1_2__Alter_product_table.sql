# --------------------------------------- #
# Alter Table Product
# --------------------------------------- #

ALTER TABLE `product`
  ADD COLUMN `aruba_placemark_id` VARCHAR(100) NULL DEFAULT ''
  AFTER `sub_category_id`;

#-----------------------#
# Create Table Placemark
#-----------------------#

CREATE TABLE IF NOT EXISTS placemark(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  placemark_id      VARCHAR(100)       NOT NULL,
  shelf_num         MEDIUMINT UNSIGNED NOT NULL,
  placemark_name    VARCHAR(100)       NOT NULL,
  updated_on        TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,

  PRIMARY KEY (id),
  INDEX (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
;


#---------------------------#
# Alter Table Product#
#---------------------------#
ALTER TABLE `product`
  ADD CONSTRAINT `FK_PRODUCT_PLACEMARK_ID` FOREIGN KEY (`aruba_placemark_id`) REFERENCES `placemark` (`id`)
  ON UPDATE CASCADE;