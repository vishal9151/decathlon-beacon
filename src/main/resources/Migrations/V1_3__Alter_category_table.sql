# --------------------------------------- #
# Alter Table Category
# --------------------------------------- #

ALTER TABLE `category`
  ADD COLUMN `image_url` TEXT NULL
  AFTER `name`;