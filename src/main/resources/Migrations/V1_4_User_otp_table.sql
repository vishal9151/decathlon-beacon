#-----------------------#
# Create Table Users
#-----------------------#

CREATE TABLE IF NOT EXISTS users(
  id                MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
  mobile            VARCHAR(12)        NOT NULL,
  otp               MEDIUMINT          NOT NULL,
  updated_on        TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  created_on        TIMESTAMP          NOT NULL,

  PRIMARY KEY (id),
  INDEX (id)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
;